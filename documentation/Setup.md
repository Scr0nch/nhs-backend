# Setup

## Prerequisites

There are a couple of things that this guide assumes the user knows:

 - How to use a terminal
 - How to follow instructions, including reading the entire sentence and related sentences *before* copy-pasting a
 terminal command
 - How to look up problems and solve problems with online resources
 - How to use your Integrated Development Environment (IDE) of choice
 - How to use the `git` command for version control
 
If you are not confident in any of the above skills, please use other better-documented online sources to increase your
proficiency in these areas before continuing.

This guide has been written for macOS and Linux users. If you are using Windows, you are mostly on your own.

You will need the username and password to the Mounds View NHS Google account to complete this guide. Contact the
necessary people to get this information if you have not already been given it.

## Package manager
Package managers are a very useful tool to easily manage software. For macOS, the recommended package manager is 
[Homebrew](https://brew.sh). If you are using Linux, you should know what package manager or software center to use. For 
Windows, I recommend switching, using the Windows Subsystem for Linux, or archaically searching online to find your 
download links and installers.

## Development environment
This project was created using [IntelliJ IDEA](https://www.jetbrains.com/idea/). Other IDEs can be used, but there is no
additional information included in this documentation on how to use them with this project. The community edition is 
available for free, but I recommend using your Mounds View student ID to apply for an educational licence for the 
professional edition because it includes database integration (a very useful feature for this project).

Note: the JetBrains Toolbox app allows you to sync and update all JetBrains applications in one place and can be
downloaded with a terminal command:

    # macOS:
    brew install jetbrains-toolbox
    
    # Arch-based Linux:
    yay jetbrains-toolbox

## Cloning the repository and installing dependencies using IntelliJ

1. Open IntelliJ and configure your settings and login into your JetBrains account or enter your licence key if you are 
using the professional version.
2. Create a new project from version control. This button should be visible on main menu dialog that opens.
3. Copy the URL of this git repository into the URL field. Enter in your username and password for GitLab if prompted.
   If your GitLab account is not authorized to clone the repository, please contact its maintainers to give you access.
4. After the project opens, a notification should appear asking if you want to import Maven Dependencies; say yes and 
enable auto import for your convenience. This project's dependencies should begin downloading.

Tip: use `Help > Find Action` (`Ctrl-Shift-A`) to find a menu item or tool (e.g. "Maven" for managing dependencies).

## Installing MariaDB
This project uses [MariaDB](https://mariadb.org/) as its database, which is similar but better than MySQL. Note that 
MariaDB conflicts with MySQL, so MySQL must be uninstalled or replaced. To install it, using the following terminal 
commands: 

macOS example:

    brew install mariadb
    # follow the on screen instructions to change the root password (make sure to remember it) and press enter on all 
    # other options to select the default one.
    mysql_secure_installation 
    mysql.server start
    # optional: check the server status
    mysql.server status
    # enter the root password you set in mysql_secure_installation, not your adminisrator/root password
    mysql -uroot -p
    # use this to save system resources when not developing: 
    mysql.server stop
    
Arch-based Linux example ([ArchWiki article](https://wiki.archlinux.org/index.php/MariaDB)):

    sudo pacman -S mariadb
    sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
    # follow the on screen instructions to change the root password (make sure to remember it) and press enter on all 
    # other options to select the default one.
    mysql_secure_installation
    sudo systemctl start  mariadb
    # optional: check the server status
    sudo systemctl status mariadb
    # enter the root password you set in mysql_secure_installation, not your adminisrator/root password
    mysql -uroot -p
    # use this to save system resources when not developing: 
    sudo systemctl stop mariadb
    
Inside the MySQL terminal, create the NHS schema. This is where the tables used in this project will be stored.

    CREATE SCHEMA nhs;
    
Now, exit the terminal:

    exit;
    
## Copying the production database to a local file
This step is optional, but useful for testing code on real data.
An alternative would be [creating the necessary tables](https://mariadb.com/kb/en/create-table/) used by this project 
manually with the correct specifications as documented in [Tables.md](Tables.md) which
may require more work.

1. To connect to the production server securely, you must use an RSA key. To generate this key, you can use the
following command or an alternative program: 

       ssh-keygen -t rsa -f rsakey

   The program will create two files in your current directory: `rsakey` and `rsakey.pub` for your private and public key.

2. Print the contents of your rsa public key to the terminal with `cat rsakey.pub` and copy the entire contents of the
file.
3. Login to the Google cloud hosting dashboard with the NHS Google Email and create an SSH session to the backend server (`162.222.176.13`). 
4. Inside this session, copy your public key from earlier onto a new line of the file at `.ssh/authorized_keys`. To do this in a terminal,
I recommend using `nano [filename]` for newer users or `vim [filename]` for users familiar with vim key bindings. Leave
a comment above your key identifying who you are.
5. Back on your local machine, create another SSH connection to the backend in a terminal outside the browser:

       ssh -i rsakey moundsviewnhs@162.222.176.13

    If you have set up your key correctly, a connection should be established.
    
6. Before making any changes to the database or copying it in this case, make a backup first:

       sudo mysqldump nhs > ~/nhs-server-backups/backup-for-nhs-[year]-[month]-[date].sql
       
   Replace the bracketed words with their proper values.
   
7. Next, securely copy the backup to your local machine:

       scp -i rsakey moundsviewnhs@162.222.176.13:~/nhs-server-backups/[name-of-backup] ~/[destination-location]/[whatever-name].sql

8. Finally, insert the database `.sql` file into your local MariaDB instance:

       sudo mysql nhs < [destination-location]/[whatever-name].sql

## Creating the secrets file
There are a couple of secrets that this application requires that are not safe to push to a remote git repository. To
provide them to the application create a text file at `src/main/java/com/moundsviewnhs/utilities/secrets.txt`. Inside
this file, a secret is defined by a key, one space character, and a value.

Copy this example secrets file with the required keys:

    database-username root
    database-password ******* 
    mail-server smtp.gmail.com
    mail-port 465
    mail-username *******@gmail.com
    mail-password *******
    
Fill in the blanks with your root database password and the mounds view NHS gmail credentials.

**Warning:** If you are using IntelliJ and have enabled automatically adding external files, the secrets file could be 
added to the git repository when you create the file! If automatic file adding has not been enabled, a prompt may appear 
asking if you want to add this file, do not! Remove this file if it ever makes it into the repository. If using
IntelliJ, the `secrets.txt` file should have a yellow color in the project menu.
 
Congratulations, you are now ready to develop inside this repository! 

In IntelliJ, simply press the green play button next to the `main` method declared in `ApplicationMain.java` to start 
the server.

Before writing any code, it is recommended to take a look at the other documentation of this project referenced above to 
familiarize yourself with this project's organization and function.

## Using IntelliJ's Database Integration Features
IntelliJ Commercial's database integration allows users to execute queries on the database, validate SQL syntax, and
get a visual representation of SQL tables among many other useful features.

To set up these features with MariaDB, follow these steps:

1. Open the Database tab (if it is not visible, it is hidden on the right)
2. Click the `+` icon to and select `Data Source > MariaDB`
3. Download and or update any drivers if prompted
4. Use `root` and the root database password you set earlier for credentials
5. Enter in `nhs` for the database name
6. Ensure the database is running and click `Test Connection` to ensure that IntelliJ can connect
7. Click `Apply` and `Ok` to save your changes

If SQL Query Syntax highlighting (on strings annotated with `@Language("MariaDB")`) throws an error, you may need to 
adjust some settings in `Settings > Languages and Frameworks > SQL Resolution Scopes`. Modify the project mapping to have only
the `nhs` schema selected (uncheck `All Schemas`) and set the path of `src/main/java` to have a resolution scope of
only the `nhs` schema (it should be the exact same as the project mapping).
