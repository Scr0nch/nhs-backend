# `CORSRoutes.java`

## `OPTIONS /* GUEST`
Sets the Cross Origin Resource Sharing headers necessary for the user's web browser to allow the frontend to make 
requests to the backend services.
    
- `200 OK`: always