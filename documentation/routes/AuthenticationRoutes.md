# `AuthenticationRouteCollection.java`

## `POST /insertAccessCode ADMIN`
Inserts the given access code with default user information values for an NHS member into the database.

    + accessCode string, non-null, non-blank, length = 7, valid access code
    
- `400 BAD REQUEST`: if the given access code does not meet the requirements

      "Access code must [not be blank / have a length of 7]!"

- `500 INTERNAL SERVER ERROR`: if inserting the user into the database failed

      "Failed to insert user!"
      
- `200 OK`: if inserting the user into the database succeeded

      "User inserted."
      
## `POST /register GUEST`
Inserts the given user details into the database.

    + firstName string, non-null, non-blank, length <= 30
    + lastName string, non-null, non-blank, length <= 30
    + grade int, non-null, 0 = teacher, 9-12 = student
    + email string non-null, non-blank, length <= 64
    + accessLevel int, non-null, = STUDENT, TEACHER, or NHS_MEMBER
    + accessCode string, non=null, non-blank, length = 7, valid access code
    + studentID string, non-null, parsable as integer, length = 6
    + password string
    
- `400 BAD REQUEST`: if any of the required JSON parameters do not meet its requirements or if the user has already registered

      "[First Name / Last Name / Email / Access Code / Student ID] must [not be blank / have a length [of / less than [value]]]!"
      "Grade must be within 9-12!"
      "Invalid access level given!"
      "User has already registered!"
      "Student ID must be a number!"
      "The given email is already in use!"
      "Password must [not be blank / be less than {MINIMUM_PASSWORD_LENGTH} characters long]"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error or other error occurred

      "Unexpected access level: [invalid access level]"
      "NHS Member not inserted into database!"
      "Failed to register user!"
      "Failed to send verification email and reset account, please contact support!"
      "Failed to send verification email!"
      
- `200 OK`: if user registration was successful

      "User registered."
      
## `POST /verifyEmail STUDENT`
Verifies the email of the user with the given access code.

    + accessCode string, non-null, non-blank, length = 7, valid access code
    
- `400 BAD REQUEST`: if the given access code does not meet its requirements

      "Access code must [not be blank / have a length of 7]!"
      
- `403 FORBIDDEN`: if the given access code is invalid for the user's email

      "Invalid access code given!"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error occurs, or the email is not registered

      "User verified email but is not in database!"
      "Failed to retrieve user details!"
      
- `200 OK`: if email verification is successful

      + (no name) non-null, serialized JSON of user data (see JSONSerializer.java for details)
      
## `POST /resendVerificationEmail STUDENT`
Resends a verification email to the user's email.

- `500 INTERNAL SERVER ERROR`: if there was an internal error when sending the verification email

      "Failed to send verification email!"
      
- `200 OK`: if sending the verification email is successful

      "Verification email sent."
      
## `POST /login GUEST`
Logs a user in using with an authentication token if the given credentials are correct.

    + email string, non-null, non-blank, length <= 64
    + password string, non-null
    
- `303 SEE OTHER`: if the user's email has not been verified

      "Email must be verified!"
      * token string, non-null, valid authentication token
    
- `400 BAD REQUEST`: if the given email or password do not meet its requirements

      "Email must [not be blank / have a length less than 64]!"
      "Invalid authentication details given!"
    
- `401 UNAUTHORIZED`: if the given credentials are incorrect

      "Email/Password combination incorrect!"
      
- `500 INTERNAL SERVER ERROR`: if a database connection or other error occurs
 
      "Failed to authenticate user!"
      "Failed to refresh user session details!"
       
- `200 OK`: if logging in the user is successful

      + (no name) non-null, serialized JSON of user data (see JSONSerializer.java for details)
      * token string, non-null, valid authentication token
      
## `POST /ping STUDENT`
Updates the last active time of users and returns a fresh authentication token.

    * token string, non-null, valid authentication token

- `500 INTERNAL SERVER ERROR`: if a database error occurs

      "Failed to refresh user session details!"
      
- `200 OK`: if the ping is successful

      "Pong"
      * token string, non-null, valid authentication token
      
## `POST /forgotPassword GUEST`
Sends a verification email to the given email to reset the user's password.

    + email string, string non-null, non-blank, length <= 64
    
- `400 BAD REQUEST`: if the given email does not meet its requirements

      "Email must [not be blank / have a length less than 64]!"
      
- `500 INTERNAL SERVER ERROR`: if a database access error occurs, or the verification email fails to send

      "Failed to get id of user with given email!"
      "Failed to send verification email!"
      
- `200 OK`: if the verification email sends successfully

      "Password reset email sent."
      
## `POST /resetPassword GUEST`
Resets the password of the user with the given email and access code to the given password.

      + email string, string non-null, non-blank, length <= 64
      + accessCode string, non=null, non-blank, length = 7, valid access code
      + password string, non-null, non-blank
      
- `400 BAD REQUEST`: if any of the required parameters does not meet its requirements

      "[Email / Access Code / Password] must [not be blank / have a length [of / less than [value]]]!"
      "Invalid access code!"
      "Password must [not be blank / be less than {MINIMUM_PASSWORD_LENGTH} characters long]"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error occurs

      "Failed to retrieve user details!"
      "Failed to reset password!"
      
- `200 OK`: if the password reset is successful

      "Password reset."