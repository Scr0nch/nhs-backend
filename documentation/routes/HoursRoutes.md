# `HoursRouteCollection.java`

## `GET /hours ADMIN`
Gets the hours of all students. Restricted to admin access to prevent data leakage.

- `204 NO CONTENT`: if there are no hours in the database

      "No hours in database!"

- `500 INTERNAL SERVER ERROR`: if a database connection or parsing error occurs

      "Failed to retrieve student hours!"
      "Failed to parse student hours!"
      
- `200 OK`: if retrieving the student hours is successful

      + (no name) string, valid JSON list of student hours (see JSONSerializer.java for details)

## `GET /hours/{studentDatabaseID} NHS_MEMBER`
Gets the hours of the student with the specified database ID.

- `400 BAD REQUEST`: if the given database ID is invalid

      "Invalid database ID!"

- `500 INTERNAL SERVER ERROR`: if a database connection error occurs

      "Failed to retrieve student hours!"
      
- `200 OK`: if retrieving the student hours is successful

      + (no name) string, valid JSON of student hours (see JSONSerializer.java for details)

## `POST /hours/{studentDatabaseID} NHS_MEMBER`
Adds an entry to the user's list of volunteering hours with the given details.

    + type: int, 0=mandatory, 1=volunteering, 2=paddock, 3=private, 4=other tutoring
    + minutes: int, > 0
    + quarter: int, 1-4
    + description: string, length <= 100
    
- `400 BAD REQUEST`: if the given database ID is invalid or any given argument does not meet its requirements

      "Invalid database ID!"
      "[Type / Quarter] must be within 0-4!"
      "Minutes must be greater than 0!"
      "Description must not [be blank / have a length less than 100]"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error occurs

      "Failed to add hours to student!"
      
- `200 OK`: if adding the student's hours is successful

      "Hours added."

## `PATCH /hours/{studentDatabaseID}/{hoursLogID} NHS_MEMER`
Edits an existing entry in the user's list of volunteering hours with the given details.

    + type: int, 0=mandatory, 1=volunteering, 2=paddock, 3=private, 4=other tutoring
    + minutes: int, > 0
    + quarter: int, 1-4
    + description: string, length <= 100
    
- `400 BAD REQUEST`: if the given database ID or hours log ID is invalid or any given argument does not meet its requirements

      "Invalid [database ID / log ID]!"
      "[Type / Quarter] must be within 0-4!"
      "Minutes must be greater than 0!"
      "Description must not [be blank / have a length less than 100]"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error occurs

      "Failed to edit student hours!"
      
- `200 OK`: if editing the student's hours is successful

      "Hours edited."

## `DELETE /hours/{studentDatabaseID}/{hoursLogID} NHS_MEMBER`
Deletes the given hours entry in the student's list of volunteering hours.

- `400 BAD REQUEST`: if the given database ID or hours log ID is invalid

      "Invalid [database ID / log ID]!"
      
- `500 INTERNAL SERVER ERROR`: if a database connection error occurs

      "Failed to delete student hours!"
      
- `200 OK`: if deleting the student's hours is successful

      "Hours deleted."