# `DebugRouteCollection.java`

## `GET /test GUEST`
Performs a simple server test; returns a hello world string.
    
- `200 OK`: always
    
      "Hello world!"
        
## `GET /testDatabase ADMIN`
Performs a simple database test; returns the user with the given database ID in JSON format. This route requires an 
admin access level so that user information cannot be leaked by non-privileged users.
    
    ? id integer, non-null, > 0
    
- `400 BAD REQUEST`: if the query parameter `id` does not meet its requirements
- `500 INTERNAL SERVER ERROR`: if a database connection error or user parsing error occurs
- `200 OK`: if no user with the given database ID exists

      "No user with the given ID!"
      
- `200 OK`: if a user with the given database ID exists

      + (no name) non-null, serialized JSON of user data
    