# Routes Documentation

Each `RouteCollection` implementation should have an associated markdown documentation class detailing in the below
format the routes that it adds to the server. Authentication-related and JSON-parsing tokens/errors can be omitted from 
documentation for readability/brevity.

## Markdown Format
    # `NameOfRouteCollectionClass.java`
    
    ## `HTTP Method /path/to/route ACCESS LEVEL REQUIRED`: (e.g. `POST login GUEST`)
    description
        
        ? requestQueryParameterName type, restrictions (e.g. ? id int, > 0)
          ...
        + requestJSONBodyFieldName type, restrictions (e.g. email string, non-null)
          ...
        * requestCookieName type, restrictions (e.g. token string, non-null)
          ...
          
    - `Status Code and NAME`: reason for occurring (e.g.` 500 INTERNAL SERVER ERROR`: failed to connect to database)
        
          "Text content"
            OR
          + responseJSONBodyFieldName type, restrictions (e.g. userData string, non-null)
            ...
          * responseCookieName type, restrictions (e.g. token string, non-null)
            ...
        ...
    - Always: (if there are parts of a response that always occur regardless of the status code)
          + responseJSONBodyFieldName type, restrictions
            ...
          * responseCookieName type, restrictions
            ...
    ...