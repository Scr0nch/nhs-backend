### Users table
| name | type | nullable | other details | comments |
| ---- |:----:|:--------:|:-------------:|:--------:|
| id | int | no | primary key, auto-incremented | handled by MariaDB; unique identified for each user
| first_name | varchar(30) | no
| last_name | varchar(30) | no
| grade | int | no | | -1=unregistered, 9-12=student, 0=teacher, 13+ graduated
| email | varchar(64) | no | unique
| access_level | int | no | | 0=student, 1=nhs member, 2=teacher, 3=admin, -1=guest
| access_code  | varchar(7) | yes | | used for registration, email verification, and password resets
| student_id | varchar(6) | no | | currently unused
| password | varchar(119) | no | | hashed and salted
| salt | varchar(16) | no
| authentication_token | varchar(32) | yes | unique | used in cookie authentication 
| last_active | timestamp | no | default=CURRENT_TIMESTAMP()
| email_verified | tinyint (bool) | no | default=false
| deleted | tinyint (bool) | no | default=false

### Hours table
| name | type | nullable | other details | comments |
| ---- |:----:|:--------:|:-------------:|:--------:|
| id          | int(11)      | no   | primary key, auto-incremented
| type        | int(2)       | no   | | 0=mandatory, 1=volunteering, 2=paddock, 3=private, 4=other tutoring
| minutes     | int(4)       | no   |
| admin_id    | int(11)      | no   | | the user ID of the admin who coordinated the volunteering
| user_id     | int(11)      | no   |
| quarter     | int(1)       | no   | | 1-4
| description | varchar(100) | yes  |
| time        | timestamp    | no   | default=current_timestamp()
| hidden      | int(1)       | no   | default=0
