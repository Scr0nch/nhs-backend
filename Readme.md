# NHS Backend

## Overview
This project is an HTTP server that provides services to the NHS Frontend through a REST API. Currently, implemented
services include authentication and simple testing.

## Documentation
Documentation can be found in `package-info.java`s for packages, in javadoc comments above all classes, methods, and 
functions that are part of a public API, and in the [documentation](documentation) folder in markdown files for routes,
setup, and SQL table structures.

## Setup
To download and install the necessary programs and code needed to develop this project, see the 
[setup](documentation/Setup.md) documentation.