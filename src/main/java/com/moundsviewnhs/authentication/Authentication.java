package com.moundsviewnhs.authentication;

import com.moundsviewnhs.database.*;
import com.moundsviewnhs.model.Response;
import com.moundsviewnhs.utilities.Argon2Utils;
import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.SecureStringFactory;
import com.moundsviewnhs.utilities.Utils;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;
import org.intellij.lang.annotations.Language;

import java.nio.charset.Charset;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Optional;

/**
 * Singleton that handles the application logic of user authentication. Guaranteed to be safe to use by and end user if
 * the correct method is chosen for the correct context (e.g. the login method is used when a user is logging in).
 * <br>Inserting:
 * {@link #insertUser(String)}
 * <br>Registration:
 * {@link Authentication#registerUser(String, String, int, String, int, String, String, char[])}.
 * <br>Logging in:
 * {@link Authentication#loginUser(HttpServerExchange, String, char[])}.
 * <br>Verifying email:
 * {@link Authentication#sendVerificationEmail(String, int)} and {@link Authentication#verifyEmail(String, String)}
 * <br>Authentication:
 * {@link Authentication#authenticateUserWithAccessLevel(HttpServerExchange, AccessLevel)} or {@link
 * Authentication#authenticateUserWithUnverifiedEmail(HttpServerExchange)}
 * <br>Pinging:
 * {@link Authentication#ping(HttpServerExchange, User)}
 * <br>Resetting password:
 * {@link Authentication#sendForgotPasswordEmail(HttpServerExchange, String)} and {@link
 * Authentication#resetPassword(String, String, String)}
 */
public enum Authentication {

    INSTANCE;

    /**
     * Inserts a user into the database. This allows the user to register later with their given access code and access
     * level. The user will have an {@link AccessLevel#NHS_MEMBER} access level.
     *
     * @param accessCode the user's access code to be used during registration
     * @throws ResponseException if the given access code is blank or {@code null} or an error response is generated
     */
    public void insertUser(final String accessCode) throws ResponseException {
        Utils.requireNonBlankAndLength(accessCode, "Access code", DatabaseConstants.ACCESS_CODE_LENGTH);

        final Optional<Integer> updatedRows = DatabaseUtils.executeUpdate(
                "INSERT INTO users (first_name, last_name, grade, email, access_level, access_code, student_id, " +
                        "password, salt) VALUES ('', '', -1, '', ?, ?, '000000', '', '')",
                AccessLevel.toDatabaseID(AccessLevel.NHS_MEMBER), accessCode);

        if (updatedRows.isEmpty() || updatedRows.get() != 1) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to insert user!", true);
        }
    }

    /**
     * Update a user with the given personal information in the {@code users} table in the database. This function does
     * not authenticate the user or log them in. The user must have already been given an access code to set their
     * personal information.
     *
     * @param firstName       the first name of the user
     * @param lastName        the last name of the user
     * @param grade           the grade of the user; 9-12 for students, 13+ for graduated members, 0 for teachers
     * @param email           the email of the user
     * @param accessLevelID   the database ID of the user's access level
     * @param accessCode      the code the user was given with their physical NHS acceptance letter
     * @param studentIDString the student ID of the user (a six digit number)
     * @param password        the password of the user
     * @throws ResponseException if the given arguments do not meet their requirements or an error response is
     *                           generated
     */
    public void registerUser(final String firstName, final String lastName, final int grade, final String email,
                             final int accessLevelID, final String accessCode, final String studentIDString,
                             final char[] password) throws ResponseException {
        // validate input, none of it can be trusted
        Utils.requireNonBlankAndMaximumLength(firstName, "First name", DatabaseConstants.NAME_MAXIMUM_LENGTH);
        Utils.requireNonBlankAndMaximumLength(lastName, "Last name", DatabaseConstants.NAME_MAXIMUM_LENGTH);
        if (grade < 0 || (grade > 0 && grade < 9)) {
            // Note: if grade is invalid, it was because of user error during a MVHS student signing up, so return an
            // appropriate error message to students only.
            throw new ResponseException(StatusCodes.BAD_REQUEST, "Grade must be within 9-12!");
        }
        // email is verified, so no validation of its contents in required now, only that it is provided at all
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        // the database id of the user if one is already assigned (if the user is an NHS member)
        final int[] id = { -1 };
        final AccessLevel accessLevel = AccessLevel.fromDatabaseID(accessLevelID);
        switch (accessLevel) {
            case GUEST:
            case ADMIN:
                // guests are not allowed to register and admins register in the same way NHS members do
                // Note: this error would be the fault of the frontend, the user does not specify their access level
                throw new ResponseException(StatusCodes.BAD_REQUEST, "Invalid access level given!");
            case STUDENT:
            case TEACHER:
                // students and teachers require no extra verification on top of verifying their email
                break;
            case NHS_MEMBER:
                // students and admins must provide a valid access code
                Utils.requireNonBlankAndLength(accessCode, "Access code", DatabaseConstants.ACCESS_CODE_LENGTH);
                // ensure that the access code given is valid, if not give the client a descriptive error message
                final User user = AuthenticationUtils.getRegisteringUserWithAccessCode(accessCode);
                if (user.getGrade() != -1) {
                    // the user has already registered, a user's grade is by default -1 before registering
                    throw new ResponseException(StatusCodes.BAD_REQUEST, "User has already registered!");
                }
                id[0] = user.getID();
                break;
        }
        final int studentID;
        switch (accessLevel) {
            case TEACHER -> studentID = -1;
            case STUDENT, NHS_MEMBER -> {
                Utils.requireNonBlankAndLength(studentIDString, "Student ID", DatabaseConstants.STUDENT_ID_LENGTH);
                try {
                    studentID = Integer.parseInt(studentIDString);
                } catch (NumberFormatException ignored) {
                    throw new ResponseException(StatusCodes.BAD_REQUEST, "Student ID must be a number!");
                }
            }
            default -> throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR,
                    "Unexpected access level: " + accessLevel, true);
        }

        PasswordRequirements.verifyPassword(new String(password));
        // generate a random salt for each user
        final String salt = SecureStringFactory.INSTANCE.createSalt();
        // execute the row-insertion update on the database using the hashed password and default character set
        final String hashedPassword = Argon2Utils.hashPassword(password, salt.getBytes(Charset.defaultCharset()));
        @Language("MariaDB") final String update;
        final Object[] arguments;
        if (accessLevel == AccessLevel.NHS_MEMBER) {
            // members are given an access code and are inserted into the
            // database before registration
            // and therefore need a different database update to register
            if (id[0] < 0) {
                throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR,
                        "NHS Member not inserted into database!", true);
            }

            update = "UPDATE users SET first_name = ?, last_name = ?, grade = ?, email = ?, student_id = ?, password " +
                    "= ?, salt = ? WHERE access_code = ? AND deleted = FALSE";
            arguments = new Object[] { firstName, lastName, grade, email, studentID, hashedPassword, salt,
                    accessCode.toUpperCase() };
        } else {
            update = "INSERT INTO users (first_name, last_name, grade, email, access_level, student_id, password, " +
                    "salt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            arguments = new Object[] { firstName, lastName, grade, email, accessLevel, studentID, hashedPassword,
                    salt };
        }

        final int updatedRows;
        try {
            updatedRows = Database.INSTANCE.executeUpdate(update, arguments);
        } catch (final SQLIntegrityConstraintViolationException ignored) {
            // without parsing the error message, it is not possible to know which constraint is violated
            // the authentication_token is not being set with any queries in this method, so it is safe to assume that
            // the given email is a duplicate
            throw new ResponseException(StatusCodes.BAD_REQUEST, "The given email is already in use!");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ResponseException(DatabaseConstants.DATABASE_ACCESS_ERROR_RESPONSE, true);
        }
        if (updatedRows != 1) {
            // no rows could be updated either if the user was already registered or there was a database access error
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to register user!", true);
        }

        if (id[0] == -1) {
            // the id of the new user needs to be retrieved if it was just inserted (and not updated, as it is for
            // NHS members). Do it now as the new user has just been inserted.
            id[0] = AuthenticationUtils.getRegisteringUserWithAccessCode(accessCode).getID();
        }

        boolean verificationEmailSent = false;
        if (id[0] > 0) {
            // send the verification email after it has been verified as unique by the database
            try {
                AuthenticationUtils.sendEmailVerificationEmail(email, id[0]);
                verificationEmailSent = true;
            } catch (final ResponseException ignored) {}
        }

        if (!verificationEmailSent) {
            // the verification email failed to send, reset the grade back to -1 so that the user can attempt to
            // register again
            final Optional<Integer> updatedRows2 = DatabaseUtils.executeUpdate(
                    "UPDATE users SET grade = -1 WHERE id = ? AND deleted = FALSE", id[0]);
            if (updatedRows2.isEmpty() || updatedRows2.get() != 1) {
                throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR,
                        "Failed to send verification email and reset account, please contact support!", true);
            }
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to send verification email!", true);
        }
    }

    /**
     * Verifies the given email with the given access code.
     *
     * @param email      the email to verify
     * @param accessCode the access code to attempt verification with
     * @return {@code FORBIDDEN} if the access code or email is incorrect, {@code BAD_REQUEST} if the access code is
     * invalid, {@code INTERNAL_SERVER_ERROR} if an exception occurs, or {@code OK} if the email was verified
     * @throws ResponseException if the given email or access code is blank or {@code null}
     */
    public User verifyEmail(final String email, final String accessCode) throws ResponseException {
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        Utils.requireNonBlankAndLength(accessCode, "Access code", DatabaseConstants.ACCESS_CODE_LENGTH);

        // null-out access code after email is verified so no other user can maliciously reset this users password
        // with the old access code
        final Optional<Integer> updatedRows = DatabaseUtils.executeUpdate(
                "UPDATE users SET email_verified = 1, access_code = NULL WHERE email = ? AND access_code = ? AND " +
                        "deleted = FALSE",
                email, accessCode.toUpperCase());
        if (updatedRows.isEmpty() || updatedRows.get() != 1) {
            throw new ResponseException(StatusCodes.FORBIDDEN, "Invalid access code given!");
        }

        // return user instance to frontend because it was not returned earlier because the user's email was not
        // verified
        return DatabaseUtils.selectUser(
                "SELECT * FROM users WHERE email = ? AND deleted = FALSE",
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "User verified email but is not in database!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve user details!"),
                email);
    }

    /**
     * Sends a verification email to the user so that they can verify their email. The email includes a securely random
     * generated access code to verify the email with. The database is updated with the new access code.
     *
     * @param email the recipient of the email
     * @param id    the database id of the user
     * @throws ResponseException if the given email is blank or {@code null}
     */
    public void sendVerificationEmail(final String email, final int id) throws ResponseException {
        // Note: input validation is handled in the utility function
        AuthenticationUtils.sendEmailVerificationEmail(email, id);
    }

    /**
     * Logs in the user with the given email and password if the given credentials are valid. Users that are valid will
     * be given an authentication token that must be sent with all future requests. This token/the users session will
     * expire after one hour of inactivity.
     *
     * @param exchange the HTTP exchange with the user's client; used to update the authentication cookie
     * @param email    the user's email
     * @param password the user's unhashed unsalted password
     * @return an HTTP response signifying the status that should be relayed to the user: {@code BAD_REQUEST} if no
     * authentication details were given, {@code UNAUTHORIZED} if the email/password combination is incorrect, {@code
     * SEE_OTHER} if the user has not verified their email, {@code INTERNAL_SERVER_ERROR} if the authentication failed,
     * or {@code OK} if the logging in was successful.
     * @throws ResponseException if the given exchange or email is blank or {@code null}
     */
    public User loginUser(final HttpServerExchange exchange, final String email, final char[] password)
            throws ResponseException {
        // TODO limit number of login attempts?
        Utils.requireNonNull(exchange, "Exchange");
        // ensure email and password are non-null
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        if (null == password) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, "Invalid authentication details given!");
        }

        // retrieve user from database with the given id; the user's salt is needed to verify the given password
        final User user = DatabaseUtils.selectUser(
                "SELECT * FROM users WHERE email = ? AND deleted = FALSE",
                // don't notify the user that the email specifically is incorrect for security purposes
                new Response(StatusCodes.UNAUTHORIZED, "Email/Password combination incorrect!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to authenticate user!"),
                email);

        if (!Argon2Utils.verifyPassword(user.getPassword(), password)) {
            throw new ResponseException(StatusCodes.UNAUTHORIZED, "Email/Password combination incorrect!");
        }

        // the user has proven that they are authentic, proceed with login
        AuthenticationUtils.setAuthenticationTokenAndCookie(exchange, user);

        if (!user.isEmailVerified()) {
            // the user must verify their email before using the website
            // Note: an authentication token is still created and the cookie is still set so that email verification
            // requests cannot be made by any other user maliciously; the email verification route requires credentials
            // Note: the frontend code will perform the redirection of the client, no "Location" header is required
            throw new ResponseException(StatusCodes.SEE_OTHER, "Email must be verified!");
        }

        // send user data back to client in response body
        return user;
    }

    /**
     * Attempts to authenticate a user with the given access level. Will throw a {@code ResponseException} containing
     * any error response generated.
     *
     * @param exchange    the HTTP exchange with the user's client; used to read the request cookies for an
     *                    authentication token.
     * @param accessLevel the minimum required access level
     * @return a valid {@link User} instance if the user has the given access level or above
     * @throws ResponseException if the given exchange is {@code null} or an error response is generated
     */
    public User authenticateUserWithAccessLevel(final HttpServerExchange exchange,
                                                final AccessLevel accessLevel) throws ResponseException {
        Utils.requireNonNull(exchange, "Exchange");
        final User user = UnsafeAuthentication.INSTANCE.authenticateUser(exchange);
        if (!user.isEmailVerified()) {
            throw new ResponseException(StatusCodes.SEE_OTHER, "Email must be verified!");
        }
        if (!AccessLevel.requireMinimum(user.getAccessLevel(), accessLevel)) {
            throw new ResponseException(StatusCodes.FORBIDDEN, "Invalid access level!");
        }
        return user;
    }

    /**
     * Attempts to authenticate a user with an unverified email. Will return an optional with a {@code null} value if
     * the user has already verified their email. Will send any error response generated before returning.
     *
     * @param exchange the HTTP exchange with the user's client; used to read the request cookies for an authentication
     *                 token.
     * @return an optional containing a partially-valid user if the user is registered and logged in, an optional with a
     * {@code null} value otherwise
     * @throws ResponseException if the given exchange is {@code null}
     */
    public User authenticateUserWithUnverifiedEmail(final HttpServerExchange exchange) throws ResponseException {
        Utils.requireNonNull(exchange, "Exchange");
        final User user = UnsafeAuthentication.INSTANCE.authenticateUser(exchange);
        if (user.isEmailVerified()) {
            // the user does not need their email verified
            throw new ResponseException(StatusCodes.SEE_OTHER, "User has already verified email!");
        }

        return user;
    }

    /**
     * Refreshes the user's authentication token, cookie, and last active time, preventing the user from having to login
     * after {@link AuthenticationConstants#AUTHENTICATION_TIMEOUT_DURATION} minutes of activity if they are interacting
     * with the frontend interface without making other backend API calls.
     *
     * @param exchange the http exchange with the user's client; used to set the user's authentication cookie
     * @param user     the user whose session is to be refreshed
     * database updates and cookie-setting were successful
     * @throws ResponseException if the given exchange or user is {@code null}
     */
    public void ping(final HttpServerExchange exchange, final User user) throws ResponseException {
        Utils.requireNonNull(exchange, "Exchange");
        Utils.requireNonNull(user, "User");
        AuthenticationUtils.setAuthenticationTokenAndCookie(exchange, user);
    }

    /**
     * Sends a verification email to the user so that they can reset their password. The email includes a securely
     * random generated access code to reset their password with. The database is updated with the new access code.
     *
     * @param exchange the HTTP exchange with the user's client; used to send an error response if one is created
     * @param email    the recipient of the email
     * @throws ResponseException if the given exchange or email is blank or {@code null}
     */
    public void sendForgotPasswordEmail(final HttpServerExchange exchange, final String email) throws ResponseException {
        Utils.requireNonNull(exchange, "Exchange");
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        // get the id of the user, if one exists, with the given email
        final User user = DatabaseUtils.selectUser(
                "SELECT * FROM users WHERE email = ? AND deleted = FALSE",
                new Response(StatusCodes.BAD_REQUEST, "Invalid email!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to get id of user with given email!"),
                email);

        AuthenticationUtils.sendVerificationEmail(email, user.getID(), "NHS: Reset password",
                "Please use the following code to reset your NHS password.");
    }

    /**
     * Resets the user with the given email and access code's password to the hash of the given password.
     *
     * @param email      the email of the user
     * @param accessCode the access code of the user
     * @param password   the new password of the user
     * @throws ResponseException if the given access code or password is blank or {@code null}
     */
    public void resetPassword(final String email, final String accessCode, final String password) throws ResponseException {
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        Utils.requireNonBlankAndLength(accessCode, "Access code", DatabaseConstants.ACCESS_CODE_LENGTH);
        if (accessCode.length() != DatabaseConstants.ACCESS_CODE_LENGTH) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, "Invalid access code!");
        }
        PasswordRequirements.verifyPassword(password);
        // get salt from user with given email and access code to hash password
        final User user = DatabaseUtils.selectUser(
                "SELECT * FROM users WHERE email = ? AND access_code = ? AND deleted = FALSE",
                new Response(StatusCodes.BAD_REQUEST, "Invalid access code!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve user details!"),
                email, accessCode.toUpperCase());

        // hash password and update database
        final String hashedPassword = Argon2Utils.hashPassword(password.toCharArray(),
                user.getSalt().getBytes(Charset.defaultCharset()));
        final Optional<Integer> updatedRows = DatabaseUtils.executeUpdate(
                "UPDATE users SET password = ?, access_code = NULL WHERE access_code = ? AND deleted = FALSE",
                hashedPassword, accessCode);
        if (updatedRows.isEmpty() || updatedRows.get() != 1) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to reset password!", true);
        }

        throw new ResponseException(StatusCodes.OK, "Password reset.");
    }

}
