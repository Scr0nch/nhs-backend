/**
 * Implements all authentication functionality, including access code and user registration, logging in, email
 * verification, logging in, cookie authentication, and password resets.
 *
 * <p>
 * Classes:
 * <ul>
 *     <li>{@link com.moundsviewnhs.authentication.Authentication}: public API for
 *     {@link com.moundsviewnhs.routes.RouteCollection}s to use in their routes. Includes convenience methods to
 *     authenticate a user in one method call.</li>
 *     <li>All other classes: package-private API that should not be used by {@code RouteCollection}s or
 *     inexperienced users. This functionality is package private for a
 * reason.</li>
 * </ul>
 */
package com.moundsviewnhs.authentication;

