package com.moundsviewnhs.authentication;

import com.moundsviewnhs.database.Database;
import com.moundsviewnhs.database.DatabaseConstants;
import com.moundsviewnhs.database.DatabaseUtils;
import com.moundsviewnhs.database.User;
import com.moundsviewnhs.model.Response;
import com.moundsviewnhs.utilities.Mail;
import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.SecureStringFactory;
import com.moundsviewnhs.utilities.Utils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.util.StatusCodes;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

/**
 * Utility class that abstracts commonly-used functionality across the authentication package. The included functions
 * may not be safe to use outside of the Authentication package in all cases.
 */
final class AuthenticationUtils {

    static User getRegisteringUserWithAccessCode(final String accessCode) throws ResponseException {
        return DatabaseUtils.selectUser("SELECT * FROM users WHERE access_code = ? AND deleted = FALSE",
                new Response(StatusCodes.BAD_REQUEST, "Invalid access code given!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to register user!"),
                accessCode.toUpperCase());
    }

    private static boolean isDuplicateAuthenticationToken(final String authenticationToken) {
        boolean[] isDuplicateAuthenticationToken = { false };
        try {
            Database.INSTANCE.executeDynamicQuery(
                    "SELECT * FROM users WHERE authentication_token = ? AND deleted = FALSE", resultSet -> {
                        try {
                            // if there is a result in the result set, the authentication token is a duplicate
                            isDuplicateAuthenticationToken[0] = resultSet.next();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }, authenticationToken);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return isDuplicateAuthenticationToken[0];
    }

    static void setAuthenticationTokenAndCookie(final HttpServerExchange exchange, final User user)
            throws ResponseException {
        // create authentication token for user to use for every request instead of email/password
        // request body authentication; it is easier to manage a single token that multiple objects
        // in a request body (login information and anything else)

        String authenticationToken;
        // ensure that the authentication token is not already in use by another user
        do {
            authenticationToken = SecureStringFactory.INSTANCE.createAuthenticationToken();
        } while (isDuplicateAuthenticationToken(authenticationToken));

        // update database with new token and updated last active time
        final Optional<Integer> updatedRows1 = DatabaseUtils.executeUpdate(
                "UPDATE users SET authentication_token = ?, last_active = ? WHERE id = ? AND deleted = 0",
                authenticationToken, Timestamp.from(Instant.now()), user.getID());
        if (updatedRows1.isEmpty() || updatedRows1.get() != 1) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to refresh user session details!", true);
        }

        // set cookie so that token authentication will be used by the browser
        final Cookie authenticationCookie = new CookieImpl(AuthenticationConstants.AUTHENTICATION_COOKIE_NAME,
                authenticationToken);
        authenticationCookie.setExpires(Date.from(Instant.now().plus(AuthenticationConstants.AUTHENTICATION_TIMEOUT_DURATION)));
        exchange.setResponseCookie(authenticationCookie);
    }

    static void sendVerificationEmail(final String email, final int id, final String subject,
                                      final String contentHeader) throws ResponseException {
        Utils.requireNonBlankAndMaximumLength(email, "Email", DatabaseConstants.EMAIL_MAXIMUM_LENGTH);
        final String accessCode = SecureStringFactory.INSTANCE.createAccessCode().toUpperCase();
        final Optional<Integer> updatedRows = DatabaseUtils.executeUpdate(
                "UPDATE users SET access_code = ? WHERE id = ? AND deleted = FALSE", accessCode, id);
        if (updatedRows.isEmpty() || updatedRows.get() != 1) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve user ID!", true);
        }
        Mail.INSTANCE.sendEmail(new String[] { email }, subject,
                contentHeader + "Your verification code is: " + accessCode + ". If you have signed up for NHS " +
                        "and you did not resend a verification email, please contact moundsviewnhs@gmail.com. If you " +
                        "have not signed up for NHS, please ignore this message as it was sent in error.");
    }

    static void sendEmailVerificationEmail(final String email, final int id) throws ResponseException {
        AuthenticationUtils.sendVerificationEmail(email, id, "NHS: Verify Email",
                "Please use the following code to verify your email for NHS.");
    }

    private AuthenticationUtils() {}

}
