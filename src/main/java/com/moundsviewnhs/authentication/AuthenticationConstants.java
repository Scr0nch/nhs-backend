package com.moundsviewnhs.authentication;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

final class AuthenticationConstants {

    static final Duration AUTHENTICATION_TIMEOUT_DURATION = Duration.of(20, ChronoUnit.MINUTES);
    static final String AUTHENTICATION_COOKIE_NAME = "token";

    private AuthenticationConstants() {}

}
