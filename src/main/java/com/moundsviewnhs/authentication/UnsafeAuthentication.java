package com.moundsviewnhs.authentication;

import com.moundsviewnhs.database.DatabaseUtils;
import com.moundsviewnhs.database.User;
import com.moundsviewnhs.model.Response;
import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.Utils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.StatusCodes;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Singleton that handles authentication logic that could be dangerously misused by an authentication API caller.
 */
enum UnsafeAuthentication {

    INSTANCE;

    /**
     * Authenticates a user's authentication token once he or she has already logged in. Ensures that the given user's
     * session has not timed out. If it has, it rejects the token an prompts them to re-login. Throws a {@code
     * ResponseException} containing any error response.
     *
     * @param exchange the HTTP exchange with the user's client; used to access the authentication cookie
     * @return a valid {@code User} instance if authentication was successful
     * @throws ResponseException if argument {@code exchange} is {@code null} or a response with a code other an {@code
     *                           OK} occurs
     */
    User authenticateUser(final HttpServerExchange exchange) throws ResponseException {
        Utils.requireNonNull(exchange, "Exchange");
        // the cookie provided by the browser is compared to the one stored in the database. If they match, the user
        // is real and can continue with their request
        final Cookie cookie = exchange.getRequestCookie(AuthenticationConstants.AUTHENTICATION_COOKIE_NAME);
        if (null == cookie) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, "No authentication token given!");
        }

        // select all users with the browser-provided authentication token. If there are any users returned, then
        // the user is authentic.
        final User user = DatabaseUtils.selectUser(
                "SELECT * FROM users WHERE authentication_token = ? AND deleted = FALSE",
                new Response(StatusCodes.UNAUTHORIZED, "Invalid authentication token given!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to authenticate user!"),
                cookie.getValue());

        if (user.getLastActiveTime().before(Timestamp.from(Instant.now().minus(1, ChronoUnit.HOURS)))) {
            // the user has timed out; they must re-login.

            // remove stale authentication token to reduce the chance of clashes, which require calculations to be
            // re-done, even though clashes are extremely unlikely
            // ignore the number of updated rows returned as this is not a critical query that is required to succeed
            DatabaseUtils.executeUpdate("UPDATE users SET authentication_token = NULL WHERE id = ? AND deleted = FALSE",
                    user.getID());

            throw new ResponseException(StatusCodes.UNAUTHORIZED,
                    "Your session has timed out due to inactivity. Please log in again.");
        }

        AuthenticationUtils.setAuthenticationTokenAndCookie(exchange, user);

        return user;
    }

}
