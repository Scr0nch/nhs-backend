package com.moundsviewnhs.authentication;

import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.Utils;
import io.undertow.util.StatusCodes;

/**
 * A utility class containing functions and constants related to verifying that a password meets this server's security
 * requirements (e.g. minimum length).
 */
final class PasswordRequirements {
    
    private static final int MINIMUM_PASSWORD_LENGTH = 10;

    /**
     * Verifies the given password against the server requirements.
     *
     * @param password the password to be verified
     * @throws ResponseException if the given password does not meet the server requirements
     */
    static void verifyPassword(final String password) {
        Utils.requireNonBlank(password, "Password");

        if (password.length() < MINIMUM_PASSWORD_LENGTH) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, "Password must be at least " + MINIMUM_PASSWORD_LENGTH + " characters long!");
        }
    }
    
}
