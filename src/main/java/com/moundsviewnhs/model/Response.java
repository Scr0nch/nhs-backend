package com.moundsviewnhs.model;

/**
 * Simple class that represents an HTTP response to a request.
 */
public class Response {

    private final int statusCode;
    private final String message;

    public Response(final int statusCode, final String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

}
