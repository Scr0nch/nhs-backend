package com.moundsviewnhs.model;

public class ServerConstants {

    // TODO place these values in a configuration file?
    public static final String SERVER_HOST = "localhost";
    public static final int SERVER_PORT = 8080;

    /**
     * The URL that the local testing instance of the frontend server is hosted at. Note that the tester must use
     * "localhost" and not their local IP address (e.g. 192.168.0.XXX) when testing for CORS requests to succeed.
     */
    public static final String TESTING_FRONTEND_SERVER_URL = "http://localhost:8081";

    /**
     * The URL that the frontend server is hosted at. Currently, this is not a valid URL because it is not hosted anywhere.
     */
    public static final String FRONTEND_SERVER_URL = "not a valid URL";

}
