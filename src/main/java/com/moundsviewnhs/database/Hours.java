package com.moundsviewnhs.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Optional;

public class Hours {

    public static Optional<Hours> fromResultSet(final ResultSet resultSet) {
        try {
            final int id = resultSet.getInt(1);
            final HoursType type = HoursType.fromDatabaseID(resultSet.getInt(2));
            final int minutes = resultSet.getInt(3);
            final int adminID = resultSet.getInt(4);
            final int userID = resultSet.getInt(5);
            final int quarter = resultSet.getInt(6);
            final String description = resultSet.getString(7);
            final Timestamp time = resultSet.getTimestamp(8);
            final boolean isHidden = resultSet.getBoolean(9);

            return Optional.of(new Hours(id, type, minutes, adminID, userID, quarter, description, time, isHidden));
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private final int id;
    private final HoursType type;
    private final int minutes;
    private final int adminID;
    private final int userID;
    private final int quarter;
    private final String description;
    private final Timestamp time;
    private final boolean isHidden;

    public Hours(final int id, final HoursType type, final int minutes, final int adminID, final int userID,
                 final int quarter, final String description, final Timestamp time, final boolean isHidden) {
        this.id = id;
        this.type = type;
        this.minutes = minutes;
        this.adminID = adminID;
        this.userID = userID;
        this.quarter = quarter;
        this.description = description;
        this.time = time;
        this.isHidden = isHidden;
    }

    public int getID() {
        return id;
    }

    public HoursType getType() {
        return type;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getAdminID() {
        return adminID;
    }

    public int getUserID() {
        return userID;
    }

    public int getQuarter() {
        return quarter;
    }

    public String getDescription() {
        return description;
    }

    public Timestamp getTime() {
        return time;
    }

    public boolean isHidden() {
        return isHidden;
    }

}
