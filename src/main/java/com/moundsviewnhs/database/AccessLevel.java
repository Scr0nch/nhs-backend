package com.moundsviewnhs.database;

/**
 * An enumeration representing all the possible access levels of a user.
 */
public enum AccessLevel {

    GUEST,
    STUDENT,
    NHS_MEMBER,
    TEACHER,
    ADMIN;

    /**
     * Converts a database ID of an AccessLevel to an AccessLevel instance.
     *
     * @param databaseID the database ID to convert
     * @return an AccessLevel instance, {@link AccessLevel#GUEST} if the given ID is invalid
     */
    public static AccessLevel fromDatabaseID(final int databaseID) {
        return switch (databaseID) {
            case 0 -> STUDENT;
            case 1 -> NHS_MEMBER;
            case 2 -> TEACHER;
            case 3 -> ADMIN;
            default -> GUEST;
        };
    }

    /**
     * Converts a AccessLevel instance to a database ID.
     *
     * @param accessLevel the access level to convert
     * @return the corresponding database ID of the given access level
     */
    public static int toDatabaseID(final AccessLevel accessLevel) {
        return switch (accessLevel) {
            case STUDENT -> 0;
            case NHS_MEMBER -> 1;
            case TEACHER -> 2;
            case ADMIN -> 3;
            default -> -1;
        };
    }

    /**
     * Tests if the given access level is at a minimum the given minimum access level.
     *
     * @param accessLevel the access level to test
     * @param minimum     the minimum access level required
     * @return {@code true} if the given access level to test is at least the given minimum access level, {@code false}
     * otherwise
     */
    public static boolean requireMinimum(final AccessLevel accessLevel, final AccessLevel minimum) {
        return toDatabaseID(accessLevel) >= toDatabaseID(minimum);
    }

}
