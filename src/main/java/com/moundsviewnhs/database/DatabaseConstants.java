package com.moundsviewnhs.database;

import com.moundsviewnhs.model.Response;
import com.moundsviewnhs.utilities.Secrets;
import io.undertow.util.StatusCodes;

import java.sql.SQLException;

public class DatabaseConstants {

    // public API constants
    public static final int NAME_MAXIMUM_LENGTH = 30;
    public static final int EMAIL_MAXIMUM_LENGTH = 64;
    /**
     * A user-readable response signifying that a database access error occurs (likely as a result of a caught {@link
     * SQLException}).
     */
    public static final Response DATABASE_ACCESS_ERROR_RESPONSE = new Response(StatusCodes.INTERNAL_SERVER_ERROR,
            "A database access error occurred!");

    // package-private constants
    // "useLegacyDatetimeCode=false" is used to disable old, weird timezone things
    static final String DATABASE_URL = "jdbc:mysql://localhost:3306/nhs?serverTimezone=UTC" +
            "&useLegacyDatetimeCode=false";
    static final String USERNAME = Secrets.INSTANCE.getSecret("database-username");
    static final String PASSWORD = Secrets.INSTANCE.getSecret("database-password");
    public static final int ACCESS_CODE_LENGTH = 7;
    public static final int STUDENT_ID_LENGTH = 6;

    private DatabaseConstants() {}

}
