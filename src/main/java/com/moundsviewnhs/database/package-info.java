/**
 * Implements all database connection functionality and contains Java classes that represent database rows in all of the
 * tables in the database.
 * <br>
 * <ul>
 *     <li>{@link com.moundsviewnhs.database.Database}: contains core connection and statement execution
 *     functionality</li>
 *     <li>{@link com.moundsviewnhs.database.DatabaseUtils}: contains convenience methods to perform database
 *     operations with less lines of redundant code
 *     </li>
 *     <li>{@link com.moundsviewnhs.database.DatabaseConstants}: contains useful constants</li>
 *     <li>All other classes: should be types representing database rows.</li>
 * </ul>
 * <p>
 */
package com.moundsviewnhs.database;