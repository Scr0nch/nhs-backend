package com.moundsviewnhs.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Optional;

/**
 * A POJO representing a row from the users table in the database. Use {@link User#fromResultSet(ResultSet)} to parse a
 * {@code User} instance from a database query.
 */
public class User {

    public static Optional<User> fromResultSet(final ResultSet resultSet) {
        // Note: MySQL databases are 1-indexed
        try {
            final int id = resultSet.getInt(1);
            final String firstName = resultSet.getString(2);
            final String lastName = resultSet.getString(3);
            final int grade = resultSet.getInt(4);
            final String email = resultSet.getString(5);
            final AccessLevel accessLevel = AccessLevel.fromDatabaseID(resultSet.getInt(6));
            final String accessCode = resultSet.getString(7);
            final String studentID = resultSet.getString(8);
            final String password = resultSet.getString(9);
            // TODO the user's salt is currently stored within the hashed password as well as in this separate column
            final String salt = resultSet.getString(10);
            final String authenticationToken = resultSet.getString(11);
            final Timestamp lastActiveTime = resultSet.getTimestamp(12);
            final boolean isEmailVerified = resultSet.getBoolean(13);
            final boolean isDeleted = resultSet.getBoolean(14);

            return Optional.of(new User(id, firstName, lastName, grade, email, accessLevel, accessCode, studentID,
                    password, salt, authenticationToken, lastActiveTime, isEmailVerified, isDeleted));
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private final int id;
    private final String firstName;
    private final String lastName;
    private final int grade;
    private final String email;
    private final AccessLevel accessLevel;
    private final String accessCode;
    private final String studentID;
    private final String password;
    private final String salt;
    private final String authenticationToken;
    private final Timestamp lastActiveTime;
    private final boolean isEmailVerified;
    private final boolean isDeleted;

    private User(final int id, final String firstName, final String lastName, final int grade, final String email,
                 final AccessLevel accessLevel, final String accessCode, final String studentID, final String password,
                 final String salt, final String authenticationToken, final Timestamp lastActiveTime,
                 final boolean isEmailVerified, final boolean isDeleted) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.grade = grade;
        this.email = email;
        this.accessLevel = accessLevel;
        this.accessCode = accessCode;
        this.studentID = studentID;
        this.password = password;
        this.salt = salt;
        this.authenticationToken = authenticationToken;
        this.lastActiveTime = lastActiveTime;
        this.isEmailVerified = isEmailVerified;
        this.isDeleted = isDeleted;
    }

    public int getID() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGrade() {
        return grade;
    }

    public String getEmail() {
        return email;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public String getStudentID() {
        return studentID;
    }

    public String getPassword() {
        return password;
    }

    public String getSalt() {
        return salt;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public Timestamp getLastActiveTime() {
        return lastActiveTime;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

}
