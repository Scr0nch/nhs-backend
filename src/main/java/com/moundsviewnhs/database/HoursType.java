package com.moundsviewnhs.database;

import com.moundsviewnhs.utilities.ResponseException;
import io.undertow.util.StatusCodes;

public enum HoursType {

    MANDATORY,
    VOLUNTEERING,
    PADDOCK,
    PRIVATE,
    OTHER_TUTORING;
    
    public static int toDatabaseID(final HoursType hoursType) {
        return switch (hoursType) {
            case MANDATORY -> 0;
            case VOLUNTEERING -> 1;
            case PADDOCK -> 2;
            case PRIVATE -> 3;
            case OTHER_TUTORING -> 4;
            default -> -1;
        };
    }
    
    public static HoursType fromDatabaseID(final int databaseID) {
        switch (databaseID) {
            case 0:
                return MANDATORY;
            case 1:
                return VOLUNTEERING;
            case 2:
                return PRIVATE;
            case 3:
                return OTHER_TUTORING;
        }
        throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Invalid hours type!", true);
    }

}
