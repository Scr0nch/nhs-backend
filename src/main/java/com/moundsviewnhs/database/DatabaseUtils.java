package com.moundsviewnhs.database;

import com.moundsviewnhs.model.Response;
import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.Utils;
import io.undertow.util.StatusCodes;
import org.intellij.lang.annotations.Language;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * A utility class that provides convenience methods that abstract error handling away from the caller when it is not
 * necessary to explicitly handle with verbose syntax.
 */
public class DatabaseUtils {

    /**
     * Creates an {@link Consumer} instance that abstracts away handling the case where there is no result from the
     * database or a {@link SQLException} occurs. The returned consumer sets the user pointer to a valid user instance
     * or throws a {@code ResponseException} with either the {@code noResultResponse} or {@code errorResponse}.
     *
     * @param userPointer      a one-length {@code User} array acting like a raw pointer
     * @param noResultResponse the response to be returned if there is no result from the database
     * @param errorResponse    the response to be returned if a {@code SQLException} occurs
     * @return a {@code Consumer} instance that populates a user pointer with a user instance
     * @throws ResponseException if {@code userPointer} is {@code null} or has a length other than {@code 1}.
     */
    public static Consumer<ResultSet> createGetUserResultSetConsumer(final User[] userPointer,
                                                                     final Response noResultResponse,
                                                                     final Response errorResponse)
            throws ResponseException {
        Utils.requireNonNull(userPointer, "User pointer");
        if (userPointer.length != 1) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "User pointer must have a length of 1!", true);
        }

        return resultSet -> {
            try {
                if (!resultSet.next()) {
                    throw new ResponseException(noResultResponse);
                }

                final Optional<User> user = User.fromResultSet(resultSet);
                if (user.isPresent()) {
                    userPointer[0] = user.get();
                    return;
                }
            } catch (final SQLException e) {
                e.printStackTrace();
            }
            throw new ResponseException(errorResponse, true);
        };
    }

    /**
     * Selects a {@link User} instance from the database with the given query. Wraps {@link
     * Database#executeDynamicQuery(String, Consumer, Object...)} with exception handling. Uses a consumer created with
     * {@link DatabaseUtils#createGetUserResultSetConsumer(User[], Response, Response)}.
     *
     * @param query            the query to execute on a database
     * @param noResultResponse the {@code Response} to return when there is no result in the database
     * @param errorResponse    the {@code Response} to return when there is a database access error {@link
     *                         SQLException}
     * @param arguments        the arguments to fill in parameters ('?') in the given statement
     * @return a valid {@code User} instance if one exists
     * @throws ResponseException if there is no result to the query or an error occurs while performing the query
     */
    public static User selectUser(@Language("MariaDB") final String query, final Response noResultResponse,
                                  final Response errorResponse, final Object... arguments) throws ResponseException {
        final User[] userPointer = new User[1];

        try {
            Database.INSTANCE.executeDynamicQuery(query,
                    DatabaseUtils.createGetUserResultSetConsumer(userPointer, noResultResponse, errorResponse),
                    arguments);
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new ResponseException(errorResponse, true);
        }

        return userPointer[0];
    }

    /**
     * Executes an update on the database with the given statement and arguments. Wraps {@link
     * Database#executeUpdate(String, Object...)} with exception handling.
     *
     * @param statement the statement to execute on the database
     * @param arguments the arguments to fill in parameters ('?') in the given statement
     * @return an optional containing a {@code Integer} if no error occurs, an empty optional otherwise
     */
    public static Optional<Integer> executeUpdate(@Language("MariaDB") final String statement,
                                                  final Object... arguments) {
        try {
            return Optional.of(Database.INSTANCE.executeUpdate(statement, arguments));
        } catch (final SQLException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    private DatabaseUtils() {}

}
