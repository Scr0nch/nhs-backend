package com.moundsviewnhs.database;

import com.moundsviewnhs.utilities.ResponseException;
import com.moundsviewnhs.utilities.Utils;
import org.intellij.lang.annotations.Language;

import java.sql.*;
import java.util.function.Consumer;

/**
 * Singleton that abstracts access to a MySQL/MariaDB database.
 */
public enum Database {

    INSTANCE;

    /**
     * Executes an unchanging query on the database. DO NOT concatenate user input into the query argument! For queries
     * with content containing user input, use {@link Database#executeDynamicQuery(String, Consumer, Object...)}. Using
     * a query with content from user input with this method opens up a SQL injection vulnerability!
     *
     * @param query          a query to execute, must not be {@code null}
     * @param resultConsumer a consumer that uses the results of the query, must not be {@code null}.
     * @throws SQLException      if a database access error occurs
     * @throws ResponseException if {@code query} or {@code resultConsumer} is {@code null}
     */
    public void executeStaticQuery(@Language("MariaDB") final String query, final Consumer<ResultSet> resultConsumer)
            throws SQLException {
        Utils.requireNonNull(query, "Query");
        Utils.requireNonNull(resultConsumer, "Result consumer");

        try (final Connection connection = DriverManager.getConnection(DatabaseConstants.DATABASE_URL,
                DatabaseConstants.USERNAME, DatabaseConstants.PASSWORD)) {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(query);
            resultConsumer.accept(resultSet);
        }
    }

    /**
     * Create a {@link PreparedStatement} instance with the given {@code String} representation and arguments. Replaces
     * all '?' characters in the given string statement with the given arguments.
     *
     * @param connection a connection to a database
     * @param statement  a String representation of a SQL statement with arguments represented as '?' characters
     * @param arguments  the arguments to replace the '?' characters in order, must not be {@code null}. The number of
     *                   arguments provided must match the number of '?' characters.
     * @return a statement that can be executed on the given database
     * @throws SQLException      if a database access error occurs
     * @throws ResponseException if {@code query}, {@code resultConsumer}, or {@code arguments} is {@code null}, or the
     *                           number of arguments provided does not match the number of '?' characters in the query.
     */
    public PreparedStatement createPreparedStatement(final Connection connection,
                                                     @Language("MariaDB") final String statement,
                                                     final Object[] arguments) throws SQLException {
        Utils.requireNonNull(connection, "Connection");
        Utils.requireNonNull(statement, "Statement");
        Utils.requireNonNull(arguments, "Arguments");


        final PreparedStatement preparedStatement = connection.prepareStatement(statement);
        // ensure the parameter count is correct
        final int parameterCount = preparedStatement.getParameterMetaData().getParameterCount();
        if (arguments.length != parameterCount) {
            throw new IllegalArgumentException("Parameter count of dynamic query (" + parameterCount + ") does " +
                    "not match the number of arguments provided (" + arguments.length + ")!");
        }

        // insert arguments into the statement
        for (int i = 1; i <= arguments.length; ++i) {
            preparedStatement.setObject(i, arguments[i - 1]);
        }

        return preparedStatement;
    }

    /**
     * Executes a query with variable components on the database. '?' characters are replaced in the query with the
     * provided variable arguments after the result consumer in order.
     *
     * @param query          a query to execute, must not be {@code null}
     * @param resultConsumer a consumer that uses the results of the query, must not be {@code null}
     * @param arguments      the arguments to replace the '?' characters in order, must not be {@code null}. The number
     *                       of arguments provided must match the number of '?' characters.
     * @throws SQLException      if a database access error occurs
     * @throws ResponseException if {@code query}, {@code resultConsumer}, or {@code arguments} is {@code null}, or the
     *                           number of arguments provided does not match the number of '?' characters in the query.
     */
    public void executeDynamicQuery(@Language("MariaDB") final String query, final Consumer<ResultSet> resultConsumer,
                                    final Object... arguments) throws SQLException {
        Utils.requireNonNull(query, "Query");
        Utils.requireNonNull(arguments, "Result consumer");
        Utils.requireNonNull(arguments, "Arguments");

        try (final Connection connection = DriverManager.getConnection(DatabaseConstants.DATABASE_URL,
                DatabaseConstants.USERNAME, DatabaseConstants.PASSWORD)) {
            final PreparedStatement preparedStatement = createPreparedStatement(connection, query, arguments);

            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultConsumer != null) {
                resultConsumer.accept(resultSet);
            }
        }
    }

    /**
     * Executes an update on the database. Similar to {@link Database#executeDynamicQuery(String, Consumer, Object...)},
     * except that this method works with updates and has no result consumer (the result is the number of rows updated
     * instead) and the former works on queries only. '?' characters are replaced in the update with the provided
     * variable arguments after the result consumer in order.
     *
     * @param update    an update to execute, must not be {@code null}
     * @param arguments the arguments to replace the '?' characters in order, must not be {@code null}. The number of
     *                  arguments provided must match the number of '?' characters.
     * @return the number of rows updated by the update
     * @throws ResponseException if {@code update} or {@code arguments} is {@code null}, or the number of arguments
     *                           provided does not match the number of '?' characters in the update.
     */
    public int executeUpdate(@Language("MariaDB") final String update, final Object... arguments) throws SQLException {
        Utils.requireNonNull(update, "Update");
        Utils.requireNonNull(arguments, "Arguments");

        try (final Connection connection = DriverManager.getConnection(DatabaseConstants.DATABASE_URL,
                DatabaseConstants.USERNAME, DatabaseConstants.PASSWORD)) {
            final PreparedStatement preparedStatement = createPreparedStatement(connection, update, arguments);

            // Note: the return value is the number of updated rows
            return preparedStatement.executeUpdate();
        }
    }

    /**
     * Allows a {@link Consumer} to use a single connection to the database. This is more efficient than multiple calls
     * to {@link Database#executeStaticQuery(String, Consumer)} and its variations because it creates only a single
     * connection to the database. If the API user needs to create a statement to execute on the database, the {@link
     * Database#createPreparedStatement(Connection, String, Object[])} utility method may be useful.
     *
     * @param connectionConsumer a consumer that uses a connection to the database
     * @throws SQLException      if a database access error occurs
     * @throws ResponseException if {@code connectionConsumer} is {@code null}
     */
    public void useConnection(final Consumer<Connection> connectionConsumer) throws SQLException {
        Utils.requireNonNull(connectionConsumer, "Connection consumer");

        try (final Connection connection = DriverManager.getConnection(DatabaseConstants.DATABASE_URL,
                DatabaseConstants.USERNAME, DatabaseConstants.PASSWORD)) {
            connectionConsumer.accept(connection);
        }
    }

}
