package com.moundsviewnhs.utilities;

import de.mkammerer.argon2.Argon2Advanced;
import de.mkammerer.argon2.Argon2Factory;

import java.nio.charset.Charset;

/**
 * Provides convenience methods for using the {@code Argon2} hashing algorithm. Use {@link
 * Argon2Utils#verifyPassword(String, char[])} to verify a password with a hash and {@link
 * Argon2Utils#hashPassword(char[], byte[])} to create a hash with a password and salt.
 */
public class Argon2Utils {

    // Note: the github example documentation recommends calculating the number of iterations for the hash function
    // dynamically. However, to make this code portable across systems with different performance abilities, reasonable
    // constants are selected instead.
    // Github repository for Argon2-JVM: https://github.com/phxql/argon2-jvm

    // Note: the documentation default for this constant is 1000ms, but one hash with the current settings goes over
    // this limit even with good hardware. Reducing this to 100ms solves this problem. This constant is unused because
    // the OPTIMAL_ITERATIONS is not calculated dynamically, but is kept for future adjustments.
    // private static final int MAXIMUM_HASH_TIME_MILLISECONDS = 100;
    // Default from Github repository readme
    private static final int MEMORY_COST_KIBIBYTES = 65536;
    // the number of threads to use when calculating a hash; four is a reasonable number for a modern processor.
    // The optimal dynamic value is Runtime.getRuntime().availableProcessors().
    private static final int PARALLELISM_THREADS = 4;

    // Note: the default hash function is Argon2i, which is not as resilient as Argon2id. Resources:
    // https://www.twelve21.io/how-to-use-argon2-for-password-hashing-in-java/
    // https://crypto.stackexchange.com/questions/48935/why-use-argon2i-or-argon2d-if-argon2id-exists
    private static final Argon2Advanced ARGON2_INSTANCE =
            Argon2Factory.createAdvanced(Argon2Factory.Argon2Types.ARGON2id);

    // Use a constant amount of iterations so the hashed passwords will be consistent regardless of initial server load
    // The optimal dynamic value is Argon2Helper.findIterations(ARGON2_INSTANCE,MAXIMUM_HASH_TIME_MILLISECONDS,
    //                                                          MEMORY_COST_KIBIBYTES, PARALLELISM_THREADS);
    private static final int OPTIMAL_ITERATIONS = 10;

    /**
     * Hashes a password using the {@code Argon2id} algorithm and wipes the password array after its completion,
     * successful or not.
     *
     * @param password the password to hash
     * @param salt     the salt to hash the password with
     * @return a {@code String} containing the hashed password and metadata.
     */
    public static String hashPassword(final char[] password, final byte[] salt) {
        try {
            return ARGON2_INSTANCE.hash(OPTIMAL_ITERATIONS, MEMORY_COST_KIBIBYTES, PARALLELISM_THREADS, password,
                    Charset.defaultCharset(), salt);
        } finally {
            ARGON2_INSTANCE.wipeArray(password);
        }
    }

    /**
     * Verifies a hashed and salted password with an unhashed and unsalted password that will be hashed with the {@code
     * Argon2id} algorithm. The given unhashed password array is wiped after the verification, successful or not.
     *
     * @param hashedPassword a hashed and salted password
     * @param password       an unhashed and unsalted password to validate
     * @return {@code true} if the given password matches the hash, {@code false} otherwise
     */
    public static boolean verifyPassword(final String hashedPassword, final char[] password) {
        try {
            return ARGON2_INSTANCE.verify(hashedPassword, password, Charset.defaultCharset());
        } finally {
            ARGON2_INSTANCE.wipeArray(password);
        }
    }
    
    private Argon2Utils() {}

}
