/**
 * Contains miscellaneous classes that implemented required but unorganized functions.
 *
 * <br>
 * Utilities:
 * <ul>
 *     <li>{@link com.moundsviewnhs.utilities.Argon2Utils}: provides convenience methods for hashing and
 * comparing salted passwords using Argon2 encryption.</li>
 *     <li>{@link com.moundsviewnhs.utilities.CORSUtils}: implements handling of Cross Origin Resource Sharing (CORS).
 *     </li>
 *     <li>{@link com.moundsviewnhs.utilities.JSONSerializer}: declares a global singleton that handles JSON
 *     serialization of any type registered to it.</li>\
 *     <li>{@link com.moundsviewnhs.utilities.Mail}: implements sending mail with the Java Mail library.</li>
 *     <li>{@link com.moundsviewnhs.utilities.ResponseException}: declares a simple `RuntimeException` that contains a
 * {@link com.moundsviewnhs.model.Response} that should be sent to the user as a result of the exception.</li>
 *     <li>{@link com.moundsviewnhs.utilities.RouteUtils}: provides various convenience methods helpful when
 *     implementing routes.</li>
 *     <li>{@link com.moundsviewnhs.utilities.Secrets}: declares a singleton that manages secrets and the {@code
 *     secrets.txt} file.</li>
 *     <li>{@link com.moundsviewnhs.utilities.SecureStringFactory}: declares a singleton that creates securely-random
 *     strings for authentication tokens, salt, and access codes</li>
 *     <li>{@link com.moundsviewnhs.utilities.Utils}: contains non-server-specific utilities</li>
 * </ul>
 */
package com.moundsviewnhs.utilities;