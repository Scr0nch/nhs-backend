package com.moundsviewnhs.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.moundsviewnhs.model.Response;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.util.AttachmentKey;
import io.undertow.util.StatusCodes;

import java.util.Optional;

/**
 * A utility class providing convenience methods relating to the management of {@link HttpServerExchange}s. It is
 * recommended that all {@link HttpHandler}s are wrapped with {@link RouteUtils#wrapHandler(HttpHandler)} before being
 * used in a {@link com.moundsviewnhs.routes.RouteCollection}.
 */
public class RouteUtils {

    /**
     * The key used to retrieve the request body in {@code HttpHandler}s passed to {@link
     * RouteUtils#wrapHandler(HttpHandler)}
     *
     * <br>
     * To retrieve the request body, pass this key as the only argument to the {@link
     * HttpServerExchange#getAttachment(AttachmentKey)} method.
     */
    public static final AttachmentKey<String> REQUEST_BODY_ATTACHMENT_KEY = AttachmentKey.create(String.class);

    /**
     * Wraps the given {@code HttpHandler} with exception and Cross Origin Resource Handling handling and reads the
     * request body. The request body's {@code InputStream} can only be read once by any handler, so reading it
     * completely the beginning of a wrapped request chain will avoid exceptions. For routes to be accessible from the
     * front end, they need a CORS response headers explicitly enabled.
     *
     *
     * <br>
     * The request body can be read by handlers passed to this function with the {@link
     * HttpServerExchange#getAttachment(AttachmentKey)} method and the {@link RouteUtils#REQUEST_BODY_ATTACHMENT_KEY}
     * constant.
     *
     * <br>
     * For example:
     * <p>
     * {@code exchange.getAttachment(RouteUtils.REQUEST_BODY_ATTACHMENT_KEY)}
     *
     * @param handler the handler to wrap with exception handling and body reading.
     * @return a new {@code HttpHandler} instance that will execute the given handler and read the request body.
     * @throws IllegalArgumentException is {@code handler} is {@code null}
     */
    public static HttpHandler wrapHandler(final HttpHandler handler) {
        if (handler == null) {
            throw new IllegalArgumentException("Argument \"handler\" must not be null!");
        }

        // blocking handler is needed for using blocking operations (e.g. reading the request body's input stream)
        return new BlockingHandler(exchange -> {
            // enable Cross Origin Resource Sharing (CORS) so that routes can be accessed from the front end
            CORSUtils.setCORSHeaders(exchange);

            // read the request body once before all other handlers; multiple requests to reading the same input
            // stream will fail
            final String requestBody =
                    new String(exchange.getInputStream().readAllBytes(), exchange.getRequestCharset());
            exchange.putAttachment(REQUEST_BODY_ATTACHMENT_KEY, requestBody);

            try {
                handler.handleRequest(exchange);
            } catch (final ResponseException e) {
                if (e.isInternal()) {
                    // prevent potential log spamming from users and only print critical exceptions
                    e.printStackTrace();
                }
                if (!exchange.isResponseStarted()) {
                    // do not send another response if it has already been started, see
                    // HttpServerExchange#setStatusCode(int) for more details
                    sendResponse(exchange, e.getResponse());
                }
            } catch (final Exception e) {
                e.printStackTrace();
                if (!exchange.isResponseStarted()) {
                    // see above comment
                    sendResponse(exchange, StatusCodes.INTERNAL_SERVER_ERROR, e.getClass().getName());
                }
            } finally {
                if (!exchange.isComplete()) {
                    // ensure that the exchange is terminated properly if the request handler did not
                    exchange.endExchange();
                }
            }
        });
    }

    /**
     * Sends the given response details off to the requester through the given exchange. Ends the exchange after these
     * details have been sent. The status code must satisfy {@code 0 <= statusCode <= 999} and the response must not
     * have been started for this exchange.
     *
     * @param exchange   the exchange in which to send the response, must not be {@code null}.
     * @param statusCode the status code to send. See {@link StatusCodes} for a list of constants and their names.
     * @param message    the message to send along with the status code. Should inform a user of the status of the
     *                   request.
     */
    public static void sendResponse(final HttpServerExchange exchange, final int statusCode, final String message) {
        exchange.setStatusCode(statusCode);
        exchange.getResponseSender().send(message != null ? message : "{}");
        exchange.endExchange();
    }

    /**
     * Creates a {@link Response} instance containing the given status code and the JSON serialization of the given
     * object as the message.
     *
     * @param exchange the exchange in which to send the response, must not be {@code null}.
     * @param statusCode the status code of the response
     * @param message    the object to serialize into response's message
     * @throws ResponseException if the given object cannot be parsed into JSON
     */
    public static void sendJSONResponse(final HttpServerExchange exchange, final int statusCode, final Object message) {
        final String jsonMessage;
        try {
            jsonMessage = JSONSerializer.INSTANCE.serialize(message);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            // return an error response if creating JSON failed
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR,
                    "Failed to create json for class: " + message.getClass(), true);
        }

        sendResponse(exchange, statusCode, jsonMessage);
    }

    /**
     * Convenience method to deserialize a Java object of a given type from the request body of a {@link
     * HttpServerExchange} and send any error response that occurs to the client.
     *
     * @param exchange    the exchange in which to send the error response if an error occurs
     * @param resultClass the class of the type of the deserialization result
     * @param <T>         the type of the deserialization result
     * @return an instance of the deserialized Java object
     * @throws ResponseException if the request body is invalid JSON for the given class
     */
    public static <T> T parseRequestBodyJSON(final HttpServerExchange exchange, final Class<T> resultClass) {
        final String requestBody = exchange.getAttachment(RouteUtils.REQUEST_BODY_ATTACHMENT_KEY);

        final Optional<T> result = JSONSerializer.INSTANCE.deserialize(requestBody, resultClass);
        if (result.isEmpty()) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, "Invalid reset password JSON given!");
        }
        return result.get();
    }

    /**
     * Convenience function for {@link RouteUtils#sendResponse(HttpServerExchange, int, String)}.
     *
     * @param exchange the exchange in which to send the response
     * @param response the response details to send
     */
    public static void sendResponse(final HttpServerExchange exchange, final Response response) {
        sendResponse(exchange, response.getStatusCode(), response.getMessage());
    }
    
    private RouteUtils() {}

}
