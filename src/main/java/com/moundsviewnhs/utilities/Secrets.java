package com.moundsviewnhs.utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Singleton that abstracts access to secrets in the secrets file. Individual secrets can be accessed with {@link
 * Secrets#getSecret(String)}.
 *
 * <br>
 * Secrets file format at {@link Secrets#SECRETS_FILE_PATH}:
 * <br>
 * secret-name (space) secret-value (newline) ...
 *
 * <br>
 * Note: only the first two words of each line are parsed, any subsequent ones will be ignored.
 * <br>
 * A line with only word will have a value of {@code null} for the given key
 */
public enum Secrets {

    INSTANCE;

    private static final String SECRETS_FILE_PATH = "src/main/java/com/moundsviewnhs/utilities/secrets.txt";

    private final Map<String, String> secrets;

    Secrets() {
        // read secrets file
        final List<String> secretsFileLines;
        try {
            secretsFileLines = Files.readAllLines(Paths.get(SECRETS_FILE_PATH));
        } catch (IOException e) {
            throw new RuntimeException("Unable to open the secrets file!", e);
        }

        // parse secrets file into map
        secrets = new HashMap<>();
        for (int i = 0, length = secretsFileLines.size(); i < length; ++i) {
            final String[] splitLine = secretsFileLines.get(i).split(" ");
            // ensure that the length of the number of strings in the line is correct for adding an entry to the map
            if (splitLine.length > 1) {
                secrets.put(splitLine[0], splitLine[1]);
            } else if (splitLine.length == 1) {
                secrets.put(splitLine[0], null);
            }
        }
    }

    /**
     * Gives the value of a secret with the given key or {@code null} if the secret has no value or does not exist.
     *
     * @param secret the key for the secret
     * @return the value of the secret in the secrets file at {@link Secrets#SECRETS_FILE_PATH}.
     */
    public String getSecret(final String secret) {
        return secrets.get(secret);
    }

}
