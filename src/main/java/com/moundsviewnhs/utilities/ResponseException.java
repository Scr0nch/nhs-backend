package com.moundsviewnhs.utilities;

import com.moundsviewnhs.model.Response;

/**
 * A {@code RuntimeException} that contains a {@link Response} to be sent to the client as a result of the exception.
 */
public class ResponseException extends RuntimeException {

    private final Response response;
    private final boolean isInternal;

    public ResponseException(final int statusCode, final String message) {
        this(new Response(statusCode, message));
    }

    public ResponseException(final Response response) {
        this(response, false);
    }
    
    public ResponseException(final int statusCode, final String message, final boolean isInternal) {
        this(new Response(statusCode, message), isInternal);
    }
    
    public ResponseException(final Response response, final boolean isInternal) {
        this.response = response;
        this.isInternal = isInternal;
    }

    public Response getResponse() {
        return response;
    }

    public boolean isInternal() {
        return isInternal;
    }

}
