package com.moundsviewnhs.utilities;

import java.security.SecureRandom;

// inspired from https://stackoverflow.com/a/48285621

/**
 * A singleton that generates randomizes strings for authentication tokens and password salt.
 * <br>Use {@link SecureStringFactory#createAuthenticationToken()} for tokens and
 * {@link SecureStringFactory#createSalt()} for salt.
 */
public enum SecureStringFactory {

    INSTANCE;

    private static final char[] ALPHANUMERIC_CHARACTERS =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    // should match the length of the authenticationToken column in the users table in the database
    private static final int AUTHENTICATION_TOKEN_LENGTH = 32;
    // should match the length of the salt column in the users table in the database
    private static final int SALT_LENGTH = 16;
    // should match the length of the accessCode column in the users table in the database
    private static final int ACCESS_CODE_LENGTH = 7;

    private final SecureRandom random = new SecureRandom();
    // Note: the length of the buffer must be the largest of AUTHENTICATION_TOKEN_LENGTH, SALT_LENGTH, and ACCESS_CODE_LENGTH
    private final char[] buffer = new char[AUTHENTICATION_TOKEN_LENGTH];

    private String createSecureString(final int length) {
        for (int i = 0; i < length; ++i) {
            buffer[i] = ALPHANUMERIC_CHARACTERS[random.nextInt(ALPHANUMERIC_CHARACTERS.length)];
        }

        return new String(buffer, 0, length);
    }

    /**
     * Generates a {@code String} containing {@link SecureStringFactory#AUTHENTICATION_TOKEN_LENGTH} securely random
     * alphanumeric characters.
     *
     * @return a new randomized authentication token
     */
    public String createAuthenticationToken() {
        return createSecureString(AUTHENTICATION_TOKEN_LENGTH);
    }

    /**
     * Generates a {@code String} containing {@link SecureStringFactory#SALT_LENGTH} securely random alphanumeric
     * characters.
     *
     * @return a new randomized salt string
     */
    public String createSalt() {
        return createSecureString(SALT_LENGTH);
    }

    /**
     * Generates a {@code String} containing {@link SecureStringFactory#ACCESS_CODE_LENGTH} securely random alphanumeric
     * characters.
     *
     * @return a new randomized access code
     */
    public String createAccessCode() {
        return createSecureString(ACCESS_CODE_LENGTH);
    }

}
