package com.moundsviewnhs.utilities;

import io.undertow.util.StatusCodes;

/**
 * A collection of convenient methods to use to prevent code duplication.
 */
public class Utils {

    /**
     * Requires that the given object is not {@code null}. Throws a {@link ResponseException} with a status code of
     * {@code INTERNAL SERVER ERROR} if the given string fails the test.
     *
     * @param object the object to test
     * @param name   the name of the object; used in the error message if the given object fails the test
     * @throws ResponseException if the given object fails the test
     */
    public static void requireNonNull(final Object object, final String name) throws ResponseException {
        if (null == object) {
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, name + " must not be null!", true);
        }
    }

    /**
     * Requires that a given {@code String} is not {@code null} or {@link String#isBlank()}. Throws a {@link
     * ResponseException} with a status code of {@code BAD REQUEST} if the given string fails the test.
     *
     * @param string the string to test
     * @param name   the name of the string; used in the error message if the given string fails the test
     * @throws ResponseException if the given string fails the tests
     */
    public static void requireNonBlank(final String string, final String name) throws ResponseException {
        if (null == string || string.isBlank()) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, name + " must not be blank!");
        }
    }

    /**
     * Requires that a given {@code String} is not {@code null} or {@link String#isBlank()} and does not exceed the
     * given maximum length. Throws a {@link ResponseException} with a status code of {@code BAD REQUEST} if the given
     * string fails the test.
     *
     * @param string        the string to test
     * @param name          the name of the string; used in the error message if the given string fails the test
     * @param maximumLength the maximum length the string can be
     * @throws ResponseException if the given string fails the tests
     */
    public static void requireNonBlankAndMaximumLength(final String string, final String name,
                                                       final int maximumLength) throws ResponseException {
        requireNonBlank(string, name);

        if (string.length() >= maximumLength) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, name + " must have a length less than " + maximumLength
                    + "!");
        }
    }

    /**
     * Requires that a given {@code String} is not {@code null} or {@link String#isBlank()} and has the given length.
     *
     * @param string the string to test
     * @param name   the name of the string; used in the error message if the given string fails the test
     * @param length the required length of the string
     * @throws ResponseException if the given string fails the tests
     */
    public static void requireNonBlankAndLength(final String string, final String name, final int length)
            throws ResponseException {
        requireNonBlank(string, name);

        if (string.length() != length) {
            throw new ResponseException(StatusCodes.BAD_REQUEST, name + " must have a length of " + length + "!");
        }
    }
    
    private Utils() {}
    
}
