package com.moundsviewnhs.utilities;

import com.moundsviewnhs.model.ServerConstants;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.HttpString;

import java.util.HashSet;
import java.util.Set;

public class CORSUtils {

    private static final Set<String> ALLOWED_ORIGINS = new HashSet<>(Set.of(ServerConstants.FRONTEND_SERVER_URL, ServerConstants.TESTING_FRONTEND_SERVER_URL));

    /**
     * For a Cross Origin Resource Sharing (CORS) request to be allowed by the user's browser the headers:
     * <ul>
     *     <li>The header "Access-Control-Allow-Origin" must be set to the frontend URL</li>
     *     <li>The header "Access-Control-Allow-Headers" must be set to any header ({@code *})</li>
     *     <li>The header "Access-Control-Allow-Credentials" must be set to {@code true}</li>
     * </ul>
     *
     * @param exchange the exchange on which to enable CORS
     */
    public static void setCORSHeaders(final HttpServerExchange exchange) {
        // return the origin of the request if it is allowed; only one allowed origin can be present in the header
        final HeaderValues requestOriginHeader = exchange.getRequestHeaders().get("Origin");
        final String requestOrigin = requestOriginHeader != null ? requestOriginHeader.getFirst() : "";
        final String allowedOrigin = ALLOWED_ORIGINS.contains(requestOrigin) ? requestOrigin : ServerConstants.FRONTEND_SERVER_URL;
        exchange.getResponseHeaders().put(new HttpString("Access-Control-Allow-Origin"), allowedOrigin);
        // prevent browser caching based on origin
        exchange.getResponseHeaders().put(HttpString.tryFromString("Vary"), "Origin");

        // allow all headers present in the request
        final StringBuilder allowedHeaders = new StringBuilder();
        final HeaderValues requestHeaders = exchange.getRequestHeaders().get("Access-Control-Request-Headers");
        if (requestHeaders != null) {
            for (int i = 0, size = requestHeaders.size(); i < size; ++i) {
                allowedHeaders.append(requestHeaders.get(i));
                if (i < size - 1) {
                    allowedHeaders.append(", ");
                }
            }
        }
        exchange.getResponseHeaders().put(new HttpString("Access-Control-Allow-Headers"), allowedHeaders.toString());

        exchange.getResponseHeaders().put(new HttpString("Access-Control-Allow-Credentials"), "true");
    }

    private CORSUtils() {}

}
