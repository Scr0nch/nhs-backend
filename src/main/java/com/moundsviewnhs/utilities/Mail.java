package com.moundsviewnhs.utilities;

import io.undertow.util.StatusCodes;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Utility singleton that abstracts sending mail with the Java mail library. Mail configuration is done in the secrets
 * file. Required options are {@code mail-server}, {@code mail-port}, {@code mail-username}, and {@code mail-password}.
 */
public enum Mail {

    INSTANCE;

    private static final String MAIL_SERVER = Secrets.INSTANCE.getSecret("mail-server");
    private static final String MAIL_PORT = Secrets.INSTANCE.getSecret("mail-port");
    private static final String MAIL_USERNAME = Secrets.INSTANCE.getSecret("mail-username");
    private static final String MAIL_PASSWORD = Secrets.INSTANCE.getSecret("mail-password");
    private static final Session SESSION;
    private static final Address MAIL_SENDING_ADDRESS;

    private static final boolean MAIL_DEBUG = false;

    static {
        final Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", MAIL_SERVER);
        properties.put("mail.smtp.port", MAIL_PORT);

        // enable SSL and authentication
        properties.put("mail.smtp.socketFactory.port", MAIL_PORT);
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        final Authenticator authenticator = new Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MAIL_USERNAME, MAIL_PASSWORD);
            }

        };

        SESSION = Session.getDefaultInstance(properties, authenticator);

        try {
            MAIL_SENDING_ADDRESS = new InternetAddress(MAIL_USERNAME);
        } catch (AddressException e) {
            throw new RuntimeException("Failed to initialize sending mail address!", e);
        }
    }

    /**
     * Sends an email to the given recipients with the given subject and content.
     *
     * @param recipients the recipients to send the email to
     * @param subject    the subject of the email
     * @param content    the plaintext content of the email
     * @throws ResponseException if {@code recipients} is {@code null}
     */
    public void sendEmail(final String[] recipients, final String subject, final String content)
            throws ResponseException {
        Utils.requireNonNull(recipients, "Recipients");
        try {
            final InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];
            for (int i = 0, length = recipients.length; i < length; ++i) {
                recipientAddresses[i] = new InternetAddress(recipients[i]);
            }
            final MimeMessage message = new MimeMessage(SESSION);
            message.setFrom(MAIL_SENDING_ADDRESS);
            message.addRecipients(Message.RecipientType.TO, recipientAddresses);
            message.setSubject(subject);
            message.setText(content);
            message.saveChanges();

            try (final Transport transport = SESSION.getTransport(MAIL_SENDING_ADDRESS)) {
                transport.connect();

                if (MAIL_DEBUG) {
                    // do not send messages when debugging mail
                    System.out.println("[Mail]: Sending message with subject: \"" + subject + "\" and content: \"" +
                            content + "\" to recipients: " + InternetAddress.toString(recipientAddresses));
                } else {
                    transport.sendMessage(message, recipientAddresses);
                }
            }
        } catch (final MessagingException e) {
            e.printStackTrace();
            throw new ResponseException(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to send email!", true);
        }
    }


}
