package com.moundsviewnhs.utilities;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.moundsviewnhs.database.AccessLevel;
import com.moundsviewnhs.database.Hours;
import com.moundsviewnhs.database.HoursType;
import com.moundsviewnhs.database.User;

import java.io.IOException;
import java.util.Optional;

/**
 * Centralized singleton that manages all JSON serialization/deserialization. Use {@link
 * JSONSerializer#serialize(Object)} to serialize a Java object. Use {@link JSONSerializer#deserialize(String, Class)}
 * to deserialize a String into a Java object.
 */
public enum JSONSerializer {

    INSTANCE;

    private final ObjectMapper objectMapper;

    JSONSerializer() {
        objectMapper = new ObjectMapper();
        // register serializers
        // User
        registerSerializer(new StdSerializer<>(User.class) {
            
            @Override
            public void serialize(final User user, final JsonGenerator jsonGenerator,
                                  final SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("id", user.getID());
                jsonGenerator.writeStringField("firstName", user.getFirstName());
                jsonGenerator.writeStringField("lastName", user.getLastName());
                jsonGenerator.writeNumberField("grade", user.getGrade());
                jsonGenerator.writeStringField("email", user.getEmail());
                jsonGenerator.writeBooleanField("isEmailVerified", user.isEmailVerified());
                jsonGenerator.writeNumberField("accessLevel", AccessLevel.toDatabaseID(user.getAccessLevel()));
                jsonGenerator.writeEndObject();
            }
            
        });
        // Hours
        registerSerializer(new StdSerializer<>(Hours.class) {

            @Override
            public void serialize(final Hours hours, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                    throws IOException {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeNumberField("id", hours.getID());
                jsonGenerator.writeNumberField("type", HoursType.toDatabaseID(hours.getType()));
                jsonGenerator.writeNumberField("minutes", hours.getMinutes());
                jsonGenerator.writeNumberField("quarter", hours.getQuarter());
                jsonGenerator.writeStringField("description", hours.getDescription());
                jsonGenerator.writeEndObject();
            }
        });
    }

    /**
     * Registers the given JSON serializer globally to be used in {@link JSONSerializer#serialize(Object)}.
     *
     * @param serializer the serializer to be registered
     */
    private void registerSerializer(final StdSerializer<?> serializer) {
        final SimpleModule userSerializerModule = new SimpleModule();
        userSerializerModule.addSerializer(serializer);
        objectMapper.registerModule(userSerializerModule);
    }

    /**
     * Serializes a given Java object into a {@code String}. Uses serializers registered with {@link
     * JSONSerializer#registerSerializer(StdSerializer)}.
     *
     * @param object the Java object to serialize
     * @return a string in JSON format representing the given Java object.
     * @throws JsonProcessingException if an error occurs while serializing JSON
     */
    public String serialize(final Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    /**
     * Deserializes the given {@code String} to an instance of the given result class.
     *
     * @param json        a string containing JSON data
     * @param resultClass the class of the Java result type
     * @param <T>         the Java type of the deserialization result
     * @return an optional containing an instance of the given result class type, or an empty optional is an {@link
     * JsonProcessingException} occurs
     */
    public <T> Optional<T> deserialize(final String json, final Class<T> resultClass) {
        try {
            return Optional.ofNullable(new ObjectMapper().readValue(json, resultClass));
        } catch (final JsonProcessingException ignored) {
            return Optional.empty();
        }
    }

}
