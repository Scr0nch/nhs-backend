package com.moundsviewnhs;

import com.moundsviewnhs.model.ServerConstants;
import com.moundsviewnhs.routes.*;
import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;

public class ApplicationMain {

    public static void main(final String[] args) {
        // define routes in a routing handler; it will manage matching the requested path in the URL to a list of routes
        final RoutingHandler routingHandler = new RoutingHandler();

        // add all routes from collections
        final RouteCollection[] routeCollections = {
                DebugRouteCollection.INSTANCE,
                AuthenticationRouteCollection.INSTANCE,
                CORSRouteCollection.INSTANCE,
                HoursRouteCollection.INSTANCE,
        };
        for (int i = 0; i < routeCollections.length; ++i) {
            final RoutingHandler collectionRoutingHandler = routeCollections[i].createRoutingHandler();
            if (collectionRoutingHandler == null) {
                throw new RuntimeException("Route collection from class \"" + routeCollections[i].getClass() + "\" " +
                        "returned a null route handler!");
            }
            routingHandler.addAll(collectionRoutingHandler);
        }

        // create server using IP constants and routing handler
        final Undertow server = Undertow.builder()
                .addHttpListener(ServerConstants.SERVER_PORT, ServerConstants.SERVER_HOST)
                .setHandler(routingHandler).build();
        server.start();

        // shutdown the server before the program closes
        Runtime.getRuntime().addShutdownHook(new Thread(server::stop));
    }

}
