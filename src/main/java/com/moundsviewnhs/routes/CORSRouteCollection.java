package com.moundsviewnhs.routes;

import com.moundsviewnhs.utilities.CORSUtils;
import com.moundsviewnhs.utilities.RouteUtils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;
import io.undertow.util.StatusCodes;

public enum CORSRouteCollection implements RouteCollection {

    INSTANCE;

    @Override
    public RoutingHandler createRoutingHandler() {
        final RoutingHandler routingHandler = new RoutingHandler();
        routingHandler.add(Methods.OPTIONS, "*", CORSRouteCollection::handleCORSPreflightRequest);
        return routingHandler;
    }

    private static void handleCORSPreflightRequest(final HttpServerExchange exchange) {
        // a browser tests to see if CORS is allowed on a resource before actually making a request
        // this response tells the user's browser that CORS is enabled
        CORSUtils.setCORSHeaders(exchange);
        RouteUtils.sendResponse(exchange, StatusCodes.OK, "");
    }

}
