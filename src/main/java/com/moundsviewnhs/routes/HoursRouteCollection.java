package com.moundsviewnhs.routes;

import com.moundsviewnhs.database.Database;
import com.moundsviewnhs.database.Hours;
import com.moundsviewnhs.utilities.RouteUtils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;
import io.undertow.util.StatusCodes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public enum HoursRouteCollection implements RouteCollection {

    INSTANCE;

    @Override
    public RoutingHandler createRoutingHandler() {
        final RoutingHandler routingHandler = new RoutingHandler();
        routingHandler.get("/hours", RouteUtils.wrapHandler(HoursRouteCollection::getHours));
        routingHandler.get("/hours/{studentDatabaseID}", RouteUtils.wrapHandler(HoursRouteCollection::getStudentHours));
        routingHandler.post("/hours/{studentDatabaseID}",
                RouteUtils.wrapHandler(HoursRouteCollection::addStudentHours));
        routingHandler.add(Methods.PATCH_STRING, "/hours/{studentDatabaseID}/{hoursLogID}",
                RouteUtils.wrapHandler(HoursRouteCollection::editStudentHours));
        routingHandler.delete("/hours/{studentDatabaseID}/{hoursLogID}",
                RouteUtils.wrapHandler(HoursRouteCollection::deleteStudentHours));
        return routingHandler;
    }

    private static void getHours(final HttpServerExchange exchange) {
        try {
            Database.INSTANCE.executeStaticQuery("SELECT * FROM hours", resultSet -> {
                try {
                    if (!resultSet.next()) {
                        RouteUtils.sendResponse(exchange, StatusCodes.NO_CONTENT, "No hours in database!");
                        return;
                    }

                    final List<Hours> hoursList = new ArrayList<>();
                    do {
                        final Optional<Hours> hours = Hours.fromResultSet(resultSet);
                        if (hours.isEmpty()) {
                            RouteUtils.sendResponse(exchange, StatusCodes.INTERNAL_SERVER_ERROR, "Failed to parse student hours!");
                            return;
                        }
                        hoursList.add(hours.get());
                    } while (resultSet.next());

                    RouteUtils.sendJSONResponse(exchange, StatusCodes.OK, hoursList);
                } catch (final SQLException e) {
                    e.printStackTrace();
                    RouteUtils.sendResponse(exchange, StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve student hours!");
                }
            });
        } catch (final SQLException e) {
            e.printStackTrace();
            RouteUtils.sendResponse(exchange, StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve student hours!");
        }
    }

    private static void getStudentHours(final HttpServerExchange exchange) {

    }

    private static void addStudentHours(final HttpServerExchange exchange) {

    }

    private static void editStudentHours(final HttpServerExchange exchange) {

    }

    private static void deleteStudentHours(final HttpServerExchange exchange) {

    }

}
