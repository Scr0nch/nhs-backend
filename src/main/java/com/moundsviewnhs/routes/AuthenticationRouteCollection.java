package com.moundsviewnhs.routes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moundsviewnhs.authentication.Authentication;
import com.moundsviewnhs.database.AccessLevel;
import com.moundsviewnhs.database.User;
import com.moundsviewnhs.utilities.RouteUtils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.StatusCodes;

public enum AuthenticationRouteCollection implements RouteCollection {

    INSTANCE;

    @Override
    public RoutingHandler createRoutingHandler() {
        final RoutingHandler handler = new RoutingHandler();
        handler.post("/insertAccessCode", RouteUtils.wrapHandler(AuthenticationRouteCollection::insertAccessCode));
        handler.post("/register", RouteUtils.wrapHandler(AuthenticationRouteCollection::registerUser));
        handler.post("/verifyEmail", RouteUtils.wrapHandler(AuthenticationRouteCollection::verifyEmail));
        handler.post("/resendVerificationEmail",
                RouteUtils.wrapHandler(AuthenticationRouteCollection::resendVerificationEmail));
        handler.post("/login", RouteUtils.wrapHandler(AuthenticationRouteCollection::login));
        handler.post("/ping", RouteUtils.wrapHandler(AuthenticationRouteCollection::ping));
        handler.post("/forgotPassword", RouteUtils.wrapHandler(AuthenticationRouteCollection::forgotPassword));
        handler.post("/resetPassword", RouteUtils.wrapHandler(AuthenticationRouteCollection::resetPassword));
        return handler;
    }

    /**
     * Stores the raw data necessary to insert a user into the users table in the database. Can be unserialized from a
     * JSON format.
     */
    private static final class InsertUserDetails {

        private final String accessCode;

        private InsertUserDetails(@JsonProperty("accessCode") final String accessCode) {
            this.accessCode = accessCode;
        }

    }

    private static void insertAccessCode(final HttpServerExchange exchange) {
        Authentication.INSTANCE.authenticateUserWithAccessLevel(exchange, AccessLevel.ADMIN);

        final InsertUserDetails insertUserDetails = RouteUtils.parseRequestBodyJSON(exchange, InsertUserDetails.class);

        Authentication.INSTANCE.insertUser(insertUserDetails.accessCode);

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "User inserted.");
    }

    /**
     * Stores the raw data received from a client to register for NHS. Data validation is implemented in the {@link
     * Authentication#registerUser(String, String, int, String, int, String, String, char[])} method. Can be
     * unserialized from a JSON format.
     */
    private static final class RegistrationDetails {

        private final String firstName;
        private final String lastName;
        private final int grade;
        private final String email;
        private final int accessLevel;
        private final String accessCode;
        private final String studentID;
        private final String password;

        private RegistrationDetails(@JsonProperty("firstName") final String firstName,
                                    @JsonProperty("lastName") final String lastName,
                                    @JsonProperty("grade") final int grade,
                                    @JsonProperty("email") final String email,
                                    @JsonProperty("accessLevel") final int accessLevel,
                                    @JsonProperty("accessCode") final String accessCode,
                                    @JsonProperty("studentID") final String studentID,
                                    @JsonProperty("password") final String password) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.grade = grade;
            this.email = email;
            this.accessLevel = accessLevel;
            this.accessCode = accessCode;
            this.studentID = studentID;
            this.password = password;
        }
    }

    private static void registerUser(final HttpServerExchange exchange) {
        final RegistrationDetails registrationDetails =
                RouteUtils.parseRequestBodyJSON(exchange, RegistrationDetails.class);

        Authentication.INSTANCE.registerUser(registrationDetails.firstName,
                registrationDetails.lastName, registrationDetails.grade, registrationDetails.email,
                registrationDetails.accessLevel, registrationDetails.accessCode, registrationDetails.studentID,
                registrationDetails.password.toCharArray());

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "User registered.");
    }

    /**
     * Stores the raw data received from a client necessary to verify a user's email. Can be unserialized from a JSON
     * format.
     */
    private static final class VerificationDetails {

        private final String accessCode;

        private VerificationDetails(@JsonProperty("accessCode") final String accessCode) {
            this.accessCode = accessCode;
        }

    }

    private static void verifyEmail(final HttpServerExchange exchange) {
        final User user = Authentication.INSTANCE.authenticateUserWithUnverifiedEmail(exchange);

        final VerificationDetails verificationDetails =
                RouteUtils.parseRequestBodyJSON(exchange, VerificationDetails.class);

        // send user data back to client in response body as JSON
        RouteUtils.sendJSONResponse(exchange, StatusCodes.OK,
                Authentication.INSTANCE.verifyEmail(user.getEmail(), verificationDetails.accessCode));
    }

    private static void resendVerificationEmail(final HttpServerExchange exchange) {
        final User user = Authentication.INSTANCE.authenticateUserWithUnverifiedEmail(exchange);

        Authentication.INSTANCE.sendVerificationEmail(user.getEmail(), user.getID());

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "Verification email sent.");
    }

    /**
     * Stores the raw data received from a client necessary to login. Can be unserialized from a JSON format.
     */
    private static class AuthenticationCredentials {

        private final String email;
        private final String password;

        private AuthenticationCredentials(@JsonProperty("email") final String email,
                                          @JsonProperty("password") final String password) {
            this.email = email;
            this.password = password;
        }

    }

    private static void login(final HttpServerExchange exchange) {
        final AuthenticationCredentials authenticationCredentials =
                RouteUtils.parseRequestBodyJSON(exchange, AuthenticationCredentials.class);

        // send user data back to client in response body as JSON
        RouteUtils.sendJSONResponse(exchange, StatusCodes.OK,
                Authentication.INSTANCE.loginUser(exchange, authenticationCredentials.email,
                        authenticationCredentials.password.toCharArray()));
    }

    private static void ping(final HttpServerExchange exchange) {
        final User user = Authentication.INSTANCE.authenticateUserWithAccessLevel(exchange, AccessLevel.STUDENT);

        Authentication.INSTANCE.ping(exchange, user);

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "Pong");
    }

    /**
     * Stores the raw data received from a client necessary to forget a user's password. Can be unserialized from a JSON
     * format.
     */
    private static class ForgotPasswordDetails {

        private final String email;

        private ForgotPasswordDetails(@JsonProperty("email") final String email) {
            this.email = email;
        }

    }

    private static void forgotPassword(final HttpServerExchange exchange) {
        // no authentication can be done because the user cannot login

        final ForgotPasswordDetails forgotPasswordDetails =
                RouteUtils.parseRequestBodyJSON(exchange, ForgotPasswordDetails.class);

        Authentication.INSTANCE.sendForgotPasswordEmail(exchange, forgotPasswordDetails.email);

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "Password reset email sent.");
    }

    /**
     * Stores the raw data received from a client necessary to reset a user's password. Can be unserialized from a JSON
     * format.
     */
    private static class ResetPasswordDetails {

        private final String email;
        private final String accessCode;
        private final String password;

        private ResetPasswordDetails(@JsonProperty("email") final String email,
                                     @JsonProperty("accessCode") final String accessCode,
                                     @JsonProperty("password") final String password) {
            this.email = email;
            this.accessCode = accessCode;
            this.password = password;
        }

    }

    private static void resetPassword(final HttpServerExchange exchange) {
        // no authentication can be done because the user cannot login

        final ResetPasswordDetails resetPasswordDetails =
                RouteUtils.parseRequestBodyJSON(exchange, ResetPasswordDetails.class);

        Authentication.INSTANCE.resetPassword(resetPasswordDetails.email, resetPasswordDetails.accessCode,
                resetPasswordDetails.password);

        RouteUtils.sendResponse(exchange, StatusCodes.OK, "Password reset.");
    }

}
