package com.moundsviewnhs.routes;

import io.undertow.server.RoutingHandler;

/**
 * An interface that define an object that creates a routing handler for a server.
 */
@FunctionalInterface
public interface RouteCollection {

    /**
     * Creates a routing handler that defines routes for a server.
     *
     * @return a {@code RoutingHandler} instance, must not be {@code null}.
     */
    RoutingHandler createRoutingHandler();

}
