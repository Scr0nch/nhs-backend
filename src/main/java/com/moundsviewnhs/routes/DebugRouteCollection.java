package com.moundsviewnhs.routes;

import com.moundsviewnhs.authentication.Authentication;
import com.moundsviewnhs.database.AccessLevel;
import com.moundsviewnhs.database.DatabaseUtils;
import com.moundsviewnhs.database.User;
import com.moundsviewnhs.model.*;
import com.moundsviewnhs.utilities.RouteUtils;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

import java.util.Deque;

public enum DebugRouteCollection implements RouteCollection {

    INSTANCE;

    @Override
    public RoutingHandler createRoutingHandler() {
        final RoutingHandler handler = new RoutingHandler();
        handler.get("/test", RouteUtils.wrapHandler(DebugRouteCollection::test));
        handler.get("/testDatabase", RouteUtils.wrapHandler(DebugRouteCollection::testDatabase));
        return handler;
    }

    private static void test(final HttpServerExchange exchange) {
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
        exchange.getResponseSender().send("Hello World!");
        exchange.endExchange();
    }

    private static void testDatabase(final HttpServerExchange exchange) {
        Authentication.INSTANCE.authenticateUserWithAccessLevel(exchange, AccessLevel.ADMIN);

        final Deque<String> idStringDeque = exchange.getQueryParameters().get("id");
        final String idString = idStringDeque == null ? null : idStringDeque.getFirst();

        if (null == idStringDeque || null == idString || "".equals(idString)) {
            RouteUtils.sendResponse(exchange, StatusCodes.BAD_REQUEST, "Query parameter \"id\" not specified!");
            return;
        }

        final int id;
        try {
            id = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            RouteUtils.sendResponse(exchange, StatusCodes.BAD_REQUEST, "Query parameter \"id\" is not a number!");
            return;
        }

        if (id < 0) {
            RouteUtils.sendResponse(exchange, StatusCodes.BAD_REQUEST,
                    "Query parameter \"id\" must be greater than 0!");
            return;
        }

        final User queryUser = DatabaseUtils.selectUser(
                "SELECT * from users WHERE id = ?",
                new Response(StatusCodes.OK, "No user with the given ID!"),
                new Response(StatusCodes.INTERNAL_SERVER_ERROR, "Failed to retrieve or parse user from database!"),
                id);

        RouteUtils.sendJSONResponse(exchange, StatusCodes.OK, queryUser);
    }

}
