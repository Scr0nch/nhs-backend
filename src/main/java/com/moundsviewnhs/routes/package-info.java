/**
 * Contains the {@link com.moundsviewnhs.routes.RouteCollection} interface and classes implementing this interface.
 * Classes implementing this interface will define routes on the server in organized groups.
 * 
 * <br>
 * Classes:
 * <ul>
 *     <li>{@code RouteCollection}: defines an object that can add multiple routes to a server</li>
 *     <li>All other classes: should implement {@code RouteCollection}</li>
 * </ul>
 */
package com.moundsviewnhs.routes;